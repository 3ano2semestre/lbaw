$(document).ready(function() {
  $("#InputConfirmPassword").keyup(validPasswordInput);


$("#settingsEmail").click(function(){
  	if(validEmailInput())
  	{
  		var emailInput = $(this).siblings(":input");
        var email = emailInput.val();
        $.ajax(updateURL, {
            type : 'POST',
            data : {
                email : email,
                userId : userId
            },
            success : function(data){
                emailInput.val('');
                emailInput.attr('placeholder', 'email successfully updated : ' + email);
            }
        });

  	}
  });

  $("#settingsPassword").click(function()
  {
  	if(validPasswordInput())
  	{
        var originalPassword = $('#InputOriginalPassword').val();
        var newPassword = $('#InputNewPassword').val();
        var confirmPassword = $('#InputConfirmPassword').val();

        $.ajax(updateURL, {
            type : 'POST',
            data : {
                originalPassword : originalPassword,
                newPassword : newPassword,
                confirmPassword : confirmPassword,
                userId : userId
            },
            success : function(data){
                var result = JSON.parse(data);
                if(result.error == false) {
                    $('#InputOriginalPassword').val('');
                    $('#InputNewPassword').val('');
                    $('#InputConfirmPassword').val('');

                    $('#InputOriginalPassword').attr('placeholder', 'Password successfully changed');
                    $('#InputNewPassword').attr('placeholder', 'Password successfully changed');
                    $('#InputConfirmPassword').attr('placeholder', 'Password successfully changed');
                } else {
                    $('#InputOriginalPassword').val('');
                    $('#InputNewPassword').val('');
                    $('#InputConfirmPassword').val('');
                    $('#InputOriginalPassword').attr('placeholder', result.message);
                    $('#InputNewPassword').attr('placeholder', result.message);
                    $('#InputConfirmPassword').attr('placeholder', result.message);
                }
            }
        });
  	}
  });

  $(".settingsQuote").click(function()
  {
  	var quoteInput = $(this).siblings(":input");
  	if(quoteInput.val().length>0){
  		quoteInput.css( 'border-color', '#00FF00' );
  		quoteInput.next(".help-block").remove();}
  	else{
  		quoteInput.css( 'border-color', '#FF0000' );
  		if(quoteInput.next("span").length==0)
    		quoteInput.after('<span for="quote" class="help-block">The quote can not be empty</span>');}
  });


});

function validPasswordInput() {
  var pass1 = $("#InputNewPassword");
  var pass2 = $("#InputConfirmPassword");
  var password1 = pass1.val();
  var password2 = pass2.val();

    if(password1 == password2) {
    	pass1.css( 'border-color', '#00FF00' );
    	pass2.css( 'border-color', '#00FF00' );
    	pass2.next(".help-block").remove();
    	return true;
    }
    else {
     	pass1.css( 'border-color', '#FF0000' );
    	pass2.css( 'border-color', '#FF0000' );
    	if(pass2.next("span").length==0)
    		pass2.after('<span for="password" class="help-block">The new passwords do not match.</span>');
    	return false;
    }   
};

function validEmailInput()
{
	var email = $("#InputEmail");
	if(IsEmail(email.val())) {
		email.css('border-color', '#00FF00');
		email.next(".help-block").remove();
		return true;
	}
	else
	{
		email.css('border-color', '#FF0000');
		if(email.next("span").length==0)
			email.after('<span for="email" class="help-block">This field is invalid.</span>');
		return false;
	}
};

function IsEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
};
