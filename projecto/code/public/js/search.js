$(document).ready(function() {
    $('.enable-disable-user').click(function(e) {
        var target = $(this).attr('user-target');
        var action = $(this).attr('action');
        var $this = $(this);
        if(action == "disable") {

            $.ajax(setEnabledURL, {
                type : 'POST',
                data : {
                    userId : target,
                    enabled : false
                },
                success : function(data){
                    $this.find("small").html('Enable user');
                    $this.attr('action', 'enable');
                }
            });
        }
        else if(action == "enable") {
            $.ajax(setEnabledURL, {
                type : 'POST',
                data : {
                    userId : target,
                    enabled : true
                },
                success : function(data){

                    $this.find("small").html('Disable user');
                    $this.attr('action', 'disable');
                }
            });
        }


    });
    $('.enable-user').click(function(e) {
        var target = $(this).attr('user-target');
        $(this).find("small").html('Disable user');
    });

    $('.dropdown-menu a').click(function (e) {
        var userId = $(this).attr('user-id');
        var privilege = $(this).attr('privilege');
        var $this = $(this);

        $.ajax(setPrivilegeURL, {
            type : 'POST',
            data : {
                userId : userId,
                privilege : privilege
            },
            success : function(data) {
                $('#privilege-' + userId).html($this.html() + ' <span class="caret">');
            }
        });
        e.preventDefault();
    })
});