<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function($table)
        {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');

            $table->boolean('active')->default('t');
            $table->integer('privilege')->default('0');

            $table->text('avatar_image')->nullable();
            $table->text('background_image')->nullable();

            $table->string('subtitle')->nullable();

            $table->string('remember_token', 100)->nullable();

            $table->timestamps();

            $table->index('username');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('users');
	}

}
