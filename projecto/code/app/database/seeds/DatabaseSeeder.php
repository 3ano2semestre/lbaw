<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$faker = Faker\Factory::create();
        $faker->addProvider(new Faker\Provider\Lorem($faker));

		for ($i = 0; $i <= 100; $i++) {
		  $user = User::create(array(
		    'username' => $faker->unique()->userName,
		    'password' => Hash::make('teste'),
		    'email' => $faker->unique()->email,
		    'privilege' => $i == 0 ? User::ADMIN_PRIVILEGE : User::USER_PRIVILEGE
		  ));
		}

        for ($i = 1; $i <= 15; $i++) {
            $collection = new Collection;
            $collection->name = implode(" ",$faker->words(rand(2,8)));
            $collection->description = implode(" ",$faker->words(rand(8,20)));
            $collection->user_id = $i;

            if (!$collection->save()) {
                continue;
            }

            for ($j = 1; $j <= 15; $j++) {
                $article = new Article;
                $article->title = implode(" ", $faker->words(rand(4,10)));
                $article->subtitle = implode(" ", $faker->sentences(rand(1,4)));
                $article->content = implode(" ", $faker->sentences(rand(5,30)));
                $article->user_id = $j;
                $article->imageurl= 'http://fin6.com/wp-content/uploads/2013/12/4627461880eb1591b56042d71759d8af6.png';

                if (!$article->save()) {
                    continue;
                }

                 for($z=1;$z<=20;$z++) {
                    $comment = new Comment;
                    $comment->article_id = $j;
                    $comment->content = implode(" ",$faker->sentences(rand(1,2)));
                    $user_id = rand(1,100);
                    $comment->user_id = $user_id;

                    $chance = rand(1,2);
                    if($chance>1 && $z>4) {
                        $parent_id=rand(1, $z-1);
                        $comment->parent_id = $parent_id;
                    }

                    if(!$comment->save()) {
                        continue;
                    }
                 }

                 $collection->articles()->attach($article->id);
            }

        }
    }

}
