<?php

	class UserController extends BaseController {

    protected $layout = 'common.master';

	public function index($id)
	{
		// Params
		$user = User::find($id);

		if(!$user || $user->active != 't') {
			App::abort(404);
		}

		$offset = Input::get('offset', 0);
		$type = Input::get('type', null);

		// Queries

		$response = "";
		if (Request::ajax() && $type == 'articles') {
			$articles = $this->getArticles($user, $offset);
			foreach($articles as $value) {
				$response .= (string)View::make('common/article', array('article' => $value));
			}

		} else if (Request::ajax() && $type == 'collections') {
			$collections = $this->getCollections($user, $offset);
			foreach($collections as $value) {
				$response .= (string)View::make('common/collection', array('collection' => $value));
			}

		} else {
			$articles = $this->getArticles($user, $offset);
			$collections = $this->getCollections($user, $offset);
		}

		// Return (JSON or HTML)

		if (Request::ajax()) {
			return Response::json($response);
		}

		$view = View::make('user/index');
		$view->user = $user;
		$view->articles = $articles;
		$view->collections = $collections;
		$this->layout->content = $view;
	}

	protected function getArticles($user, $offset)
	{
		return $user->articles()->orderBy('created_at', 'desc')->skip($offset)->take(10)->get();
	}

	protected function getCollections($user, $offset)
	{
		return $user->collections()->orderBy('created_at', 'desc')->skip($offset)->take(10)->get();
	}

	public function settings()
	{
		if(($this->user)==null)
			App::abort(401);

		$view = View::make('user/settings');
		$view->user = $this->user;
		$this->layout->content = $view;
	}

    public function update() {
        $result = array('error' => false);
        if(!Input::has('userId')) {
            $result['error'] = true;
            $result['message'] = 'No user id sents';
            $error = new Error();
            $error->user_id = $this->user->id;
            $error->content = json_encode($result);
            $error->save();
        } else if(($this->user)==null) {
            $result['error'] = true;
            $result['message'] = 'Not logged in';
            $error = new Error();
            $error->user_id = $this->user->id;
            $error->content = json_encode($result);
            $error->save();
        } else if($this->user->id != Input::get('userId')) {
            $result['error'] = true;
            $result['message'] = 'You can only edit your profile';
            $error = new Error();
            $error->user_id = $this->user->id;
            $error->content = json_encode($result);
            $error->save();
        }

        $result['updated'] = array();
        if(Input::has('email')) {
            if(filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL)) {
                $this->user->email = Input::get('email');
                array_push($result['updated'], 'email');
            }
        }

        if(Input::has('originalPassword') && Input::has('newPassword') && Input::has('confirmPassword')) {
            if(Auth::validate(array('username'=>$this->user->username, 'password' => Input::get('originalPassword')))) {
                $newPassword = Input::get('newPassword');
                $confirmPassword =  Input::get('confirmPassword');
                if($newPassword == $confirmPassword) {
                    $this->user->password = Hash::make($newPassword);
                    array_push($result['updated'], 'password');
                } else {
                    $result['error'] = true;
                    $result['message'] = 'Passwords do not match';
                    $error = new Error();
                    $error->user_id = $this->user->id;
                    $error->content = json_encode($result);
                    $error->save();
                }
            } else {
                $result['error'] = true;
                $result['message'] = 'Wrong password, try again';
                $error = new Error();
                $error->user_id = $this->user->id;
                $error->content = json_encode($result);
                $error->save();
            }
        }

        $this->user->save();

        return json_encode($result);
    }

    public function setEnabled() {
        $result = array('error' => false);

        if(!$this->user) {
            $result['error'] = true;
            $result['message'] = 'Not logged in';
            $error = new Error();
            $error->content = json_encode($result);
            $error->save();
        } else if($this->user->privilege == 1) {
            $result['error'] = true;
            $result['message'] = 'You have no permission to do that';
            $error = new Error();
            $error->user_id = $this->user->id;
            $error->content = json_encode($result);
            $error->save();
        } else if(!Input::has('userId') || !Input::has('enabled')) {
            $result['error'] = true;
            $result['message'] = 'Missing parameters';
            $error = new Error();
            $error->user_id = $this->user->id;
            $error->content = json_encode($result);
            $error->save();
        }

        if($result['error'])
            return json_encode($result);


        $user = User::find(Input::get('userId'));

        if(!$user) {
            $result['error'] = true;
            $result['message'] = 'User not found';
            $error = new Error();
            $error->user_id = $this->user->id;
            $error->content = json_encode($result);
            $error->save();
            return json_encode($result);
        }

        if(Input::get('enabled') == 'true') {
            $user->active = true;
        } else {
            $user->active = false;
        }

        $user->save();
        return json_encode($result);
    }

    public function setPrivilege() {
        $result = array('error' => false);
        if(!$this->user) {
            $result['error'] = true;
            $result['message'] = 'Not logged in';
            $error = new Error();
            $error->content = json_encode($result);
            $error->save();
        } else if($this->user->privilege != User::ADMIN_PRIVILEGE) {
            $result['error'] = true;
            $result['message'] = 'You have no permission to do that';
            $error = new Error();
            $error->user_id = $this->user->id;
            $error->content = json_encode($result);
            $error->save();
        } else if(!Input::has('userId') || !Input::has('privilege')) {
            $result['error'] = true;
            $result['message'] = 'Missing parameters';
            $error = new Error();
            $error->user_id = $this->user->id;
            $error->content = json_encode($result);
            $error->save();
        }


        if($result['error'])
            return json_encode($result);

        $privilege = Input::get('privilege');
        $user = User::find(Input::get('userId'));
        if(!$user) {
            $result['error'] = true;
            $result['message'] = 'User not found';
        } else if($privilege > User::ADMIN_PRIVILEGE || $privilege < User::USER_PRIVILEGE ) {
            $result['error'] = true;
            $result['message'] = 'Invalid privilege value';
        }

        if($result['error'])
            return json_encode($result);

        $user->privilege = $privilege;
        $user->save();

        return json_encode($result);
    }

    public function follow($id)
    {   
	   	$followee = User::find($id);
	   	$action = Input::get('type',null);
    	
    	if($action=="Follow")
    	{	
    		if($this->user->follows($followee))	
    		{
    			$result = array('success' => false);
    			$result['about']='User already followed';

                $error = new Error();
                $error->user_id = $this->user->id;
                $error->content = json_encode($result);

    			return json_encode($result);
    		}
    		else
    		{
    			$this->user->followees()->attach($followee);
    			$result = array('success' => true);
    			$result['result']='followed';
    			return json_encode($result);	
    		}

		}
    	else if($action=="Unfollow")
    	{
    		if($this->user->follows($followee))
    		{
    			$this->user->followees()->detach($followee);
    			$result = array('success' => true);
    			$result['result']='unfollowed';
    			return json_encode($result);
    		}
    		else
    		{
    			$result = array('success' => false);
    			$result['about']='User was not followed';
    			return json_encode($result);
    		}
    	}
    	else
    	{
    		$result = array('success' => false);
    		$result['about']='Invalid action';
    		return json_encode($result);
    	}
    }

    public function remove($id) {
        if(!$this->user) {
            $error = new Error();
            $error->content = ['route'=>'removeUser', 'message'=>"Not logged in"];
            $error->save();
            return Redirect::route('home');
        }
        if($this->user->id != $id) {
            $error = new Error();
            $error->content = ['route'=>'removeUser', 'message'=>"No permission"];
            $error->user_id = $this->user->id;
            $error->save();
            return Redirect::route('home');
        }


        $this->user->subscriptions()->detach();
        $this->user->followees()->detach();
        $this->user->followers()->detach();


        $votes = $this->user->commentVotes()->get();
        foreach($votes as  $v) {
            $v->delete();
        }
        $votes = $this->user->articleVotes()->get();
        foreach($votes as  $v) {
            $v->delete();
        }

        $comments = $this->user->comments()->get();
        foreach($comments as $c) {
            $votes = $c->votes()->get();
            foreach($votes as $v) {
                $v->delete();
            }
            $c->delete();
        }

        $articles = $this->user->articles()->get();
        foreach($articles as $a) {
            $comments = $a->comments()->get();
            foreach($comments as $c) {
                $votes = $c->votes()->get();
                foreach($votes as $v) {
                    $v->delete();
                }
                $c->delete();
            }
            $a->collections()->detach();
            $votes = $a->votes()->get();
            foreach($votes as $v) {
                $v->delete();
            }
            $a->delete();
        }
        $collections = $this->user->collections()->get();
        foreach($collections as $c) {
            $c->articles()->detach();
            $c->followers()->detach();
            $c->delete();
        }



        $this->user->delete();
        Auth::logout();
        return Redirect::route('home');
    }
}
