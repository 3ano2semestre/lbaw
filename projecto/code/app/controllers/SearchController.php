<?php

	class SearchController extends BaseController {

    protected $layout = 'common.master';

	public function index()
	{

        $q = Input::get('q', null);
        if (!$q) {
            return Redirect::to('home');
        }

        $type = Input::get('type', null);
        $offset = Input::get('offset', null);

        $response = "";
        $privilege = User::USER_PRIVILEGE;
        if($this->user != null) {
            $privilege = $this->user->privilege;
        }

        if (!Request::ajax()) {
            $articles = $this->getArticles($q, $offset);
            $collections = $this->getCollections($q, $offset);
            $users = $this->getUsers($q, $offset);
        } else if ($type == 'articles') {
            $objects = $this->getArticles($q, $offset);

            foreach ($objects as $object) {
                $response .= (string)View::make('common/article', array('article' => $object));
            }
        } else if ($type == 'collections') {
            $objects = $this->getCollections($q, $offset);

            foreach ($objects as $object) {
                $response .= (string)View::make('common/collection', array('collection' => $object));
            }
        } else if ($type == 'users') {
            $objects = $this->getUsers($q, $offset);

            foreach ($objects as $object) {
                $response .= (string)View::make('common/user', array('user' => $object,'privilege'=>$privilege));
            }
        }

        if (Request::ajax()) {
            return Response::json($response);
        }





        $this->layout->content = View::make('search.index');//->with('data',$data);

        $this->layout->content->articles = $articles;
        $this->layout->content->collections = $collections;
        $this->layout->content->users = $users;
        $this->layout->content->privilege = $privilege;
	}

    protected function getArticles($q, $offset)
    {
        return Article::where('title', 'LIKE', "%$q%")
            ->orWhere('subtitle', 'LIKE', "%$q%")
            ->skip($offset)
            ->take(10)
           // ->orWhere('id', '=', $q)
            ->get();
    }

    protected function getCollections($q, $offset)
    {
        return Collection::where('name', 'LIKE', "%$q%")
            ->orWhere('description', 'LIKE', "%$q%")
            ->skip($offset)
            ->take(10)
          //  ->orWhere('id', '=', $q)
            ->get();
    }

    protected function getUsers($q, $offset)
    {
        $q = User::where('username', 'LIKE', "%$q%")
            ->orWhere('email', 'LIKE', "%$q%");

        if($this->user != null) {
            if($this->user->privilege == User::USER_PRIVILEGE) {
                $q->where('active','=','t');
            }
        }

        $q->skip($offset)
            ->take(10);
          //  ->orWhere('id', '=', $q)
        return $q->get();
    }

}
