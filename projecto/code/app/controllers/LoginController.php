<?php
/**
 * Created by IntelliJ IDEA.
 * User: pedro
 * Date: 16/05/14
 * Time: 16:31
 */

class LoginController extends BaseController {


    public function index() {

        if ($this->user) {
            return Redirect::route('home');
        }

        $view = View::make('login.index');
        $view->error = Session::get('error', null);
        $view->registrationError =  Session::get('registrationError',null);
        $this->layout->content = $view;
    }

    public function login()
    {
        if(!Input::has('username') || !Input::has('password')) {
            return Redirect::route('login');
        }

        $username = Input::get('username');
        $password = Input::get('password');

        if (Auth::attempt(array('username' => $username, 'password' => $password), true)) {
            return Redirect::route('home');
        } else {
            Session::flash('error', 'Authentication Failed');
            return Redirect::route('login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::route('home');
    }

    public function register()
    {
        $user = new User;

        if ($user->validate(Input::all())) {
            $user->email = Input::get('email');
            $user->username = Input::get('username');
            $user->password = Hash::make(Input::get('password'));
            $user->active = true;
            $user->privilege = User::USER_PRIVILEGE;
            if ($user->save()) {
                return Redirect::route('home');
            }
        }

        return Redirect::route('login')->with('registrationError', $user->errors()->first())->withInput();
    }
}
