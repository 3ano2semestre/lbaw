<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    const ADMIN_PRIVILEGE = 3;
    const MODERATOR_PRIVILEGE = 2;
    const USER_PRIVILEGE = 1;

    public $rules = array(
        'username'=>'required|alpha|min:3|unique:users',
        'email'=>'required|email|unique:users',
        'password'=>'required|alpha_num|between:6,12|confirmed',
        'password_confirmation'=>'required|alpha_num'
    );

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    public function articles()
    {
        return $this->hasMany('Article');
    }

    public function collections()
    {
        return $this->hasMany('Collection');
    }

    public function articleVotes()
    {
        return $this->hasMany('ArticleVote');
    }

    public function commentVotes()
    {
        return $this->hasMany('CommentVote');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }
    public function followees()
    {
      return $this->belongsToMany('User', 'followers', 'follower_id', 'followee_id');
    }

    public function followers()
    {
      return $this->belongsToMany('User', 'followers', 'followee_id', 'follower_id');
    }

    public function subscriptions()
    {
        return $this->belongsToMany('Collection', 'subscriptions', 'user_id', 'collection_id');
    }

    public function validate($data)
    {
        $v = Validator::make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }

        return true;
    }

    public function errors() {
        return $this->errors;
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * Return gravatar URL
     * @return string
     */
    public function getGravatarUrl() {
        return "http://gravatar.com/avatar/" . md5(mb_strtolower($this->email));
    }

    /**
     * Returns name of user permission
     * @return null|string
     */
    public function getUserPermissionName() {
        $result = null;
        switch($this->privilege) {
            case User::ADMIN_PRIVILEGE:
                $result = "Admin";
                break;
            case User::MODERATOR_PRIVILEGE:
                $result = "Moderator";
                break;
            case User::USER_PRIVILEGE:
                $result = "User";
                break;
        }
        return $result;
    }

    public function follows($user) {
        $result = $this->followees()->where('id','=',$user->id)->first();
        if($result)
            return true;
        return false;
    }

    public function isSubscribedToCollection($collection) {
        $result = $this->subscriptions()->where('id','=',$collection->id)->first();
        if($result)
            return true;
        return false;
    }

    public function deleteUser() {
        $this->articles()->delete();
        $this->subscriptions()->detach();
        $this->collections()->delete();
    }
}
