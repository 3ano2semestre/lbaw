<?php
/**
 * Created by IntelliJ IDEA.
 * User: Emanuelpinho
 * Date: 16/05/14
 * Time: 16:46
 */

class CommentVote extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comment-votes';

    public function comment()
    {
        return $this->belongsTo('Comment');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

}
