@extends('common.master')

@section('content')
<div class = "container">
	<div class = "errorDiv">
		<h1 style="text-align: center;">
			Error 401!
			<br>	
			You should not be here, go away
			<br>
			<br>
			<img src="{{asset('img/access_denied.jpg')}}" height="500" width="500">
		</h1>
	</div>
<div>
@stop