@extends('common.master')

@section('content')

<!-- Profile Info -->
<div class="perfil-page info">

    <div class="well">
        <center>
            <div><img src="{{ $user->getGravatarUrl() }}" name="aboutme" width="140" height="140" class="img-circle"></div>
            <h3>{{$user->username;}}</h3>
            <em>{{$user->subtitle;}}</em>
        </center>
    </div>


	<!-- Feed -->

    <div class="container">
        @if(Auth::check())
            @if(Auth::user()->follows($user))
                <button type="button" class="btn btn-default js-follow" id="followButton" followID="{{$user->id}}" >Unfollow</button>
            @else
                @if(Auth::user()->id != $user->id)
                  <button type="button" class="btn btn-default js-follow" id="followButton" followID="{{$user->id}}" >Follow</button>
                @endif
            @endif
          @endif
        <ul id="page-tabs-nav">
            <li style="cursor:pointer" class="active" data-content="articles">Published stories</li>
            <li style="cursor:pointer" data-content="collections">Collections</li>
        </ul>
    </div>
    <div class="container">
		<ul class="timeline js-placeholder articles active" data-type="articles">
    		@foreach ($articles as $article)
    			@include ('common.article', array('article'=>$article))
    		@endforeach
            @if (!count($articles))
            <div class="well">
                No published stories
            </div>
            @else
            @include ('common.loadmore-button')
            @endif
		</ul>
		<ul class="timeline js-placeholder collections" data-type="collections" hidden>
    		@foreach ($collections as $collection)
    			@include ('common.collection', array('collection'=>$collection))
    		@endforeach
            @if (!count($collections))
            <div class="well">
                No published collections
            </div>
            @else
            @include ('common.loadmore-button')
            @endif
		</ul>
	</div>
</div>

@include ('common.activate-loadmore')

@stop
