<script>
    var addCommentURL = '{{URL::route('addComment', array('id'=>$article->id))}}';
    var deleteCommentURL = '{{URL::route('deleteComment', array('id'=>$article->id))}}';
    var voteCommentURL = '{{URL::route('voteComment', array('id'=>$article->id))}}';
    @if($user)
    var userPicture = "{{$user->getGravatarUrl()}}";
    var username = "{{$user->username}}"
    @endif
</script>
<div class="detailBox">
    <div class="titleBox">
        <label>Comment Box</label>
        <button type="button" class="close" aria-hidden="true">&times;</button>
    </div>

    <div class="actionBox">
        <ul class="commentList"  id="commentsBox">
            @foreach($comments as $c)
            <li id="comment-{{ $c->id }}">
                <div class="commenterImage">
                    <img src="{{$c->user->getGravatarUrl()}}" />
                </div>
                <div class="commentText">
                    <p class="">{{ $c->content }}</p>
                    <span class="date sub-text" data-livestamp="{{date('U',strtotime($c->created_at))}}"></span>
                    <span class="sub-text">by {{$c->user->username}}</span>
                    <a href="#" class="replyComment" target="{{ $c->id }}" id="href-reply-{{ $c->id }}">
                        <span class="sub-text">reply</span>
                    </a>
                    @if($user)
                        @if($c->user->id == $user->id)
                            <a href="#" class="deleteComment" target="{{ $c->id }}" id="href-delete-{{ $c->id }}">
                                <span class="sub-text">delete</span>
                            </a>
                            <span id="comment-confirmation-{{ $c->id }}" style="display:none">
                                <span class="sub-text">are you sure?</span>
                                <a href="#" class="confirmDeleteComment" target="{{ $c->id }}" id="href-delete-{{ $c->id }}">
                                    <span class="sub-text">yes</span>
                                </a>
                                <a href="#" class="cancelDeleteComment" target="{{ $c->id }}" id="href-delete-{{ $c->id }}">
                                    <span class="sub-text">no</span>
                                </a>
                            </span>
                        @else
                            <?php  $color = 0; ?>
                            @if(isset($votes[$c->id]))
                                @if($votes[$c->id] == 1)
                                    <?php $color = 1; ?>
                                @endif
                                @if($votes[$c->id] == -1)
                                    <?php $color = -1; ?>
                                @endif
                            @endif
                            <a href="#" class="voteComment" target="{{ $c->id }}" id="href-voteup-{{ $c->id }}" value="1">
                                <span class="sub-text" id="span-voteup-{{ $c->id }}"
                                    @if($color == 1)
                                        style="color:green"
                                    @endif
                                    >+{{$c->getPlusVotes()}}</span>
                            </a>
                            <a href="#" class="voteComment" target="{{ $c->id }}" id="href-votedown-{{ $c->id }}" value="-1">
                                <span class="sub-text" id="span-votedown-{{ $c->id }}"
                                    @if($color == -1)
                                        style="color:red"
                                    @endif

                                    >-{{$c->getMinusVotes()}}</span>
                            </a>
                        @endif
                    @endif
                    <form class="form-inline" role="form" style="display:none;" id="form-reply-{{ $c->id }}">
                        <div class="form-group">
                            <input class="form-control" type="text" id="input-reply-{{ $c->id }}" placeholder="Your reply" id="newReplyBox" />
                        </div>
                        <div class="form-group">
                            <button class="btn btn-default newReplyBtn" target="{{ $c->id }}">Add</button>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger cancelReply" target="{{ $c->id }}">Cancel</button>
                        </div>
                    </form>

                    <ul id="replies-{{ $c->id }}">
                    @foreach($c->childComments()->get() as $cc)
                        <li id="comment-{{ $cc->id }}">
                            <div class="commenterImage">
                                <img src="{{$cc->user->getGravatarUrl()}}" />
                            </div>
                            <div class="commentText">
                                <p class="">{{ $cc->content }}</p>
                                <span class="date sub-text" data-livestamp="{{date('U',strtotime($c->created_at))}}"></span>
                                <span class="sub-text">by {{$cc->user->username}}</span>
                                @if($user)
                                @if($cc->user->id == $user->id)
                                    <a href="#" class="deleteComment" target="{{ $cc->id }}" id="href-delete-{{ $cc->id }}">
                                        <span class="sub-text">delete</span>
                                    </a>
                                    <span id="comment-confirmation-{{ $cc->id }}" style="display:none">
                                        <span class="sub-text">are you sure?</span>
                                        <a href="#" class="confirmDeleteComment" target="{{ $cc->id }}" id="href-delete-{{ $c->id }}">
                                            <span class="sub-text">yes</span>
                                        </a>
                                        <a href="#" class="cancelDeleteComment" target="{{ $cc->id }}" id="href-delete-{{ $cc->id }}">
                                            <span class="sub-text">no</span>
                                        </a>
                                    </span>
                                @endif
                                @endif

                            </div>
                         </li>
                    @endforeach

                    </ul>
                </div>
            </li>
            @endforeach
            <!--<li>
                <div class="commenterImage">
                    <img src="http://lorempixel.com/50/50/people/6" />
                </div>
                <div class="commentText">
                    <p class="">Hello this is a test comment.</p> <span class="date sub-text">on March 5th, 2014</span>
                </div>
            </li>
            -->
        </ul>
        <form class="form-inline" role="form">
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Your comment" id="newCommentBox" />
            </div>
            <div class="form-group">
                <button class="btn btn-default" id="newCommentBtn">Send</button>
            </div>
        </form>
    </div>
</div>