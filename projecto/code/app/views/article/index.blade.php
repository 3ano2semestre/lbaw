@extends('common.master')

@section('content')

<script>
    var updateArticleUrl = "{{{URL::route('updateArticle', array('id'=>$article->id))}}}";
    var addToCollection = "{{{URL::route('addToCollection', array('id'=>$article->id))}}}";
    var removeFromCollection = "{{{URL::route('removeFromCollection', array('id'=>$article->id))}}}";
</script>

@if (is_null($article->imageurl))
<div class="article-header">
@else
<div class="article-header" style="background: url('{{{asset($article->imageurl)}}}') center center no-repeat fixed;">
@endif
    <div class="container" style="position: relative">
    <!-- Article Header -->
        <div class="article-title">
          <h1 id="title"
              @if($editable)
                contenteditable="true"
              @endif
              >{{$article->title}}</h1>
          <h2 id="subtitle" <?php if($editable): ?> contenteditable="true" <?php endif; ?>>
              <small>
                  {{$article->subtitle}}
              </small>
          </h2>
            <h5>{{$articleUser->subtitle;}}</h5>
        </div>
    </div>
  </div>

  <div class="container">
    <div  class="article <?php if($editable): ?> toTinyMCE <?php endif; ?>" id="content-area">
     {{$article->content}}
    </div>

    <div class="btn-toolbar" role="toolbar">
      <div class="btn-group btn-group-sm">
        <button type="button" class="btn btn-default js-vote-count">{{ $voteCount }}</button>
        @if ($user)
        <button type="button" class="btn btn-default js-vote @if ($voted == 1) active @endif" data-value="1">
          <i class="glyphicon-chevron-up glyphicon"></i>
        </button>
        <button type="button" class="btn btn-default js-vote @if ($voted == -1) active @endif" data-value="-1">
          <i class="glyphicon-chevron-down glyphicon"></i>
        </button>
        @endif
      </div>
      <div class="btn-group btn-group-sm">
        <a href="{{ URL::to('user/' . $articleUser->id ) }}">
          <button type="button" class="btn btn-default btn-sm">
              From: {{ $articleUser->username }}
          </button>
        </a>
      </div>
      @if($editable)
      <div class="btn-group btn-group-sm">
        <button type="button" id="add-collection-button" class="btn btn-default add-to-collection" data-toggle="modal" data-target="#colletions-modal">
            Add to collection
        </button>
      </div>
      <div class="btn-group btn-group-sm">
        <a id="remove-article-button" class="btn btn-danger btn-sm" data-toggle="confirmation" data-placement="right" data-original-title="" title="" href="{{ URL::to('article/delete/' . $article->id) }}">Remove</a>
        </a>
      </div>
      @endif

    </div>
  </div>
</div>

<!-- Add to Collections Modal -->
@if($editable)
<div class="modal fade" id="colletions-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Collections</h4>
            </div>
            <div class="modal-body">
                @foreach($collections as $c)
                        <div class="row collections-row collections-container">
                            <div class="col-md-4">
                                <h3>
                                    {{ $c->name }}
                                </h3>
                                <h20>
                                    {{ $c->description }}
                                </h20>
                            </div>
                            @if (in_array($c['id'], $articlesInCollection))
                                <div class="col-md-4 col-md-offset-4 add-collection-button">
                                    <button id="collection-subscribe" data-toggle="collection-subscribe" target="{{$c->id}}" type="button" class="btn btn-success glyphicon glyphicon-thumbs-up collection-subscribe"></button>
                                </div>
                            @else
                                <div class="col-md-4 col-md-offset-4 add-collection-button">
                                    <button id="collection-unsubscribe" data-toggle="collection-unsubscribe" target="{{$c->id}}" type="button" class="btn btn-default glyphicon glyphicon-plus-sign collection-unsubscribe"></button>
                                </div>
                            @endif
                        </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

<!-- Comments -->
<div class="container">
    <div  class="article" >
        @include('article.comments', array('article'=>$article, 'comments'=>$comments, 'user'=>$user, 'votes'=>$votes))
    </div>
</div>
@stop
