
<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <button type="button" class="navbar-toggle" data-toggle="collapse"
        data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <div class="navbar-header">
      <a class="navbar-brand" href="{{ URL::route('home') }}">{wabl}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="{{ URL::route('home') }}">Home</a></li>

        @if (isset($user) && $user)
        <li><a href="{{ URL::route('createArticle') }}">Create Article</a></li>
        <li><a href="{{ URL::route('createCollection') }}">New Collection</a></li>
        @endif
      </ul>

      <ul class="nav navbar-nav navbar-right">
            @if (isset($user) && $user)
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                User <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li><a href="{{ URL::route('profile', array('id'=>$user->id)) }}">Profile</a></li>
                <li><a href="{{ URL::route('settings') }}">Settings</a></li>
                <li><a href="{{ URL::route('logout') }}" style="padding-right: 0">Logout</a></li>
              </ul>
            </li>
            @else
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ URL::route('login') }}" style="padding-right: 0">Login</a></li>
            </ul>
            @endif

           <form class="navbar-form navbar-right" role="search" action="{{ URL::route('search') }}" style="margin-left:0; margin-right:0">
                <div class="form-group">
                    <input type="text" name="q" class="form-control" placeholder="Search"  style="border: 1px solid #555; background: none" value="{{ Input::get('q') }}">
                </div>
            </form>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
