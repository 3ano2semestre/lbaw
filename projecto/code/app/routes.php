<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Home
Route::get('/', array('as'=>'home', 'uses'=>'HomeController@index'));

//
Route::get('login',  array('as' => 'login', 'uses'=>'LoginController@index'));
Route::get('logout',  array('as' => 'logout', 'uses'=>'LoginController@logout'));
Route::post('login', array('as'=>'login', 'uses'=>'LoginController@login'));
Route::post('register', array('as'=>'register', 'uses'=>'LoginController@register'));

Route::get('user/{id}', array('as'=>'profile', 'uses'=>'UserController@index'))->where('id', '[0-9]+');
Route::get('user/settings', array('as'=>'settings','uses'=>'UserController@settings'));
Route::post('user/settings', array('as'=>'settings','uses'=>'UserController@settings'));
Route::post('user/{id}/getMoreStories/{articleNumber}','UserController@getMoreStories')->where(array('id'=> '[0-9]+','articleNumber'=>'[0-9]+'));
Route::post('user/{id}/getMoreCollections/{collectionNumber}', 'UserController@getMoreCollections')->where(array('id'=> '[0-9]+','collectionNumber'=>'[0-9]+'));
Route::post('user/update', array('as' => 'updateUser', 'uses' => 'UserController@update'));
Route::post('user/setenabled', array('as' => 'setEnabledUser', 'uses' => 'UserController@setEnabled'));
Route::post('user/setprivilege', array('as' => 'setUserPrivilege', 'uses' => 'UserController@setPrivilege'));
Route::post('user/{id}/follow',array('as'=>'followUser', 'uses'=>'UserController@follow'))->where('id', '[0-9]+');
Route::get('user/{id}/remove',array('as'=>'removeUser', 'uses'=>'UserController@remove'))->where('id', '[0-9]+');


Route::get('article/{id}', array('as'=>'article', 'uses'=>'ArticleController@index'))->where('id', '[0-9]+');
Route::get('article/delete/{id}', array('as'=>'deleteArticle', 'uses' => 'ArticleController@delete'))->where('id', '[0-9]+');
Route::post('article/{id}/comment', array('as' => 'addComment', 'uses' => 'ArticleController@addComment'))->where('id', '[0-9]+');
Route::delete('article/{id}/comment', array('as'=>'deleteComment', 'uses' => 'ArticleController@removeComment'));
Route::post('article/{id}/comment/vote', array('as'=>'voteComment', 'uses' => 'ArticleController@voteComment'));

Route::get('search', ['as' => 'search', 'uses' => 'SearchController@index']);
Route::get('article/create', array('as' => 'createArticle', 'uses' =>'ArticleController@create'));
Route::post('article/create', 'ArticleController@insert');
Route::post('article/update/{id}', array('as'=>'updateArticle', 'uses'=>'ArticleController@update'))->where('id', '[0-9]+');
Route::post('article/addToCollection/{id}', array('as'=>'addToCollection', 'uses'=>'ArticleController@addToCollection'))->where('id', '[0-9]+');
Route::post('article/removeFromCollection/{id}', array('as'=>'removeFromCollection', 'uses'=>'ArticleController@removeFromCollection'))->where('id', '[0-9]+');

Route::get('collection/create', array('as' => 'createCollection', 'uses'=>'CollectionController@create'));
Route::post('collection/create', array('as' => 'insertCollection', 'uses'=>'CollectionController@insert'));
Route::get('collection/edit/{id}', array('as' => 'editCollection', 'uses'=>'CollectionController@edit'))->where('id', '[0-9]+');
Route::post('collection/edit/{id}', array('as' => 'saveCollection', 'uses'=>'CollectionController@save'))->where('id', '[0-9]+');
Route::get('collection/{id}', array('as'=>'collection', 'uses'=>'CollectionController@index'))->where('id', '[0-9]+');
Route::delete('collection/{id}', 'CollectionController@delete')->where('id', '[0-9]+');
Route::post('collection/{id}/getMoreArticles/{numberArticle}', 'CollectionController@getMoreArticles')->where(array('id'=> '[0-9]+','collectionNumber'=>'[0-9]+'));
Route::get('collection/delete/{id}', array('as'=>'deleteCollection', 'uses' => 'CollectionController@delete'))->where('id', '[0-9]+');
Route::post('collection/{id}/subscribe',array('as'=>'followCollection', 'uses'=>'CollectionController@subscribe'))->where('id', '[0-9]+');



Route::get('test', 'TestModelsController@index');
