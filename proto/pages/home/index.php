<?php
    include_once('../../config/init.php');
    include_once($BASE_DIR .'database/users.php');

    if (isLogged()) {
        header('Location: ' . $BASE_URL . 'pages/feed');
    }

    $smarty->display('home/index.tpl');
