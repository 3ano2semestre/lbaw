<?php
	include_once('../../config/init.php');
    include_once('../../database/users.php');
    include_once('../../database/article.php');

	if (!($user = isLogged())) {
        header('Location: ' . $BASE_URL);
    }

    $smarty->assign('user', $user);

    $smarty->display('feed/index.tpl');
