<?php
    include_once('../../config/init.php');
    include_once($BASE_DIR .'database/users.php');

    if (isset($_POST['login']) && $_POST['login']) {

        if (isset($_POST['username']) && isset($_POST['password']) && $_POST['username'] && $_POST['password']) {
            if (login($_POST['username'], $_POST['password'])) {
                header('Location: ' . $BASE_URL . 'pages/feed/');
            }
        }

        $smarty->assign('login_error', 'Login failed');

    } else if (isset($_POST['signup']) && $_POST['signup']) {

        if ($_POST['password'] == $_POST['passwordConfirmation']) {

            if (createUser($_POST['username'], $_POST['password'])) {
                login($_POST['username'], $_POST['password']);
                header('Location: ' . $BASE_URL . 'pages/feed/');
            } else {
                $smarty->assign('signup_error', 'Sign up failed');
            }


        } else {
            $smarty->assign('signup_error', 'Password confirmation failed');
        }
    }

    $smarty->display('users/login.tpl');
