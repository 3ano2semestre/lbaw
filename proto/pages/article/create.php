<?php
    include_once('../../config/init.php');
    include_once('../../database/users.php');
    include_once('../../database/article.php');
    if (!isLogged()) {
        header('Location: ' . $BASE_URL);
    }
    
    if(isset($_POST['title']) && isset($_POST['content'])) {

        if(isset($_POST['private-article'])) {
            $active = 0;
        }
        else {
            $active = 1;
        }
        //die(json_encode($_FILES["image"]));
        move_uploaded_file($_FILES["image"]["tmp_name"],"../../images/assets/" . $_FILES["image"]["name"]);
    	insertArticle($_POST['title'], $_POST['subtitle'], $_FILES["image"]["name"],
         $_POST['content'], $_SESSION['id'], $active);
    	header('Location: ' . $BASE_URL . 'pages/feed/');
    }

    $smarty->display('article/create.tpl');
