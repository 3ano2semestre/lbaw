<?php
    include_once('../../config/init.php');
    include_once('../../database/users.php');
    include_once('../../database/article.php');

    if (!($user = isLogged())) {
        header('Location: ' . $BASE_URL);
    }

    $smarty->assign('user', $user);

    if(!isset($_GET['id']) || !$_GET['id']) {
	    echo "teste, sem id";
  	} else {
      $article = getArticle($_GET['id']);

      $smarty->assign('article', $article[0]);
      $smarty->assign('user', $user);

      $smarty->display('article/view.tpl');
    }

