<?php

	function getArticle($id = null){
		global $conn;
$id = "1";
		$stmt = $conn->prepare('SELECT * FROM "Articles" WHERE id = ?');
		$stmt->execute(array($id));
		//die("asf");
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	function insertArticle($title, $subtitle, $image, $content, $user_id, $active = true) {
		global $conn;
		//die($title . " - " . $subtitle . " - " . $image . " - " . $content . " - " . $user_id . " - " . $active . " - ");
		$stmt = $conn->prepare('INSERT INTO "Articles" (title, image, content, user_id, active,  created_at) VALUES (?, ?, ?, ?, ?, ?)');
		$stmt->execute(array($title,  $image, $content, $user_id, $active, date('Y-m-d H:i:s')));
		//return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	function getAllArticles() {
		global $conn;

		$stmt = $conn->prepare('SELECT * FROM "Articles"');
		$stmt->execute();
		
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}