<?php

function login($username, $password)
{
    $id = "";
    if ($user = isLoginCorrect($username, $password)) {
        $_SESSION['id'] = $user['id'];
        return true;
    }

  return false;
}

function createUser($username, $password)
{
    global $conn;
    $stmt = $conn->prepare('INSERT INTO "Users" (username, password) VALUES (?, ?)');
    return $stmt->execute(array($username, sha1($password)));
}

function isLoginCorrect($username, $password)
{
    global $conn;
    $stmt = $conn->prepare('SELECT * FROM "Users" WHERE username = ? AND password = ?');
    $stmt->execute(array($username, sha1($password)));

    return $stmt->fetch();
}

function isLogged()
{
    if (isset($_SESSION['id']) && $_SESSION['id']) {
        global $conn;
        $stmt = $conn->prepare('SELECT * FROM "Users" WHERE id = ?');
        $stmt->execute(array($_SESSION['id']));

        return $stmt->fetch();
    }

    return false;
}

function logout()
{
    unset($_SESSION['id']);
}

