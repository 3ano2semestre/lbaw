{include file='common/header.tpl'}

<div class="container">
<form action="#" method="POST" enctype="multipart/form-data">

    <div class="col-xs-12 article create-article">
        <input name="title" class="search-input title" type="text" placeholder="Title" />
        <input name="sub" class="search-input sub-title" type="text" placeholder="Sub-title" />
        <img src="{$BASE_URL}images/assets/add_image.png" class="add-image">
        <input type="file" name="image" id="image" >
        <textarea class="search-input story" rows="4" cols="50" name="content" placeholder="Write your story"></textarea>
    </div>
    <div class="col-xs-12 article">
        <input type="submit" value="Submeter" />
        <input type="checkbox" name="private-article" value="private" id="private-article" />
        <label for="private-article">Privado</label>
    </div>
</form>
</div>

{include file='common/footer.tpl'}

