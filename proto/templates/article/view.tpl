{include file='common/header.tpl'}

{if $article.image}
<div class="article-header" style="background: url('{$BASE_URL}{$article.image}') center center no-repeat fixed">
{else}
<div class="article-header">
{/if}
    <div class="container">
    <!-- Article Header -->
        <div class="article-title">
          <h1>{$article.title}</h1>
          <h2><small>{$article.subtitle}</small></h2>
        </div>
    </div>
  </div>

  <div class="container">
    <div class="col-xs-12 article">
      {$article.content}
    </div>
  </div>
</div>

{include file='common/footer.tpl'}
