<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="../../assets/ico/favicon.ico" />

  <title>{if $title}{$title}{else}WABL{/if}</title>

  <!-- Bootstrap core CSS -->
  <link href="{$BASE_URL}css/bootstrap.min.css" rel="stylesheet" type="text/css" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
  </script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js">
  </script>
  <![endif]-->

  <!-- Custom styles for this template -->
  <link href="{$BASE_URL}css/carousel.css" rel="stylesheet" type="text/css" />
  <link href="{$BASE_URL}css/style.css" rel="stylesheet">
</head>

<body>

  <!-- Menu -->
  <div class="mainMenu">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container"  style="max-width: 950px;">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=
          ".navbar-collapse"><span class="sr-only">Toggle navigation</span></button>
          <a class="navbar-brand" href="index.html">wabL</a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="{$BASE_URL}">Home</a></li>
                {if $user}
                    <li><a href="create_article.html">Create Article</a></li>
                    <li><a href="#about">New Collection</a></li>
                {/if}
            </ul>

            <ul class="nav navbar-nav navbar-right">
                {if $user}
                    <li><a href="{$BASE_URL}pages/users/logout.php">Logout</a></li>
                {/if}
            </ul>

            <form class="navbar-form navbar-right" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search"  style="border: 1px solid #555; background: none">
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
