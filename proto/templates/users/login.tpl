{include file='common/header.tpl'}

<div class="container login-container">
      <div class="row">
          <div class ="col-lg-7 sign-in">
              <h3> Welcome to Wabl </h3>
              <!--  <div class="row fb-sign">
                  <h5> Sign in here </h5> or <img src="img/fblogin.png" width="250" />
              </div> -->
              {if $login_error}
                <h4 style="color: red">{$login_error}</h4>
              {/if}
              <form method="POST" enctype="multipart/form-data">
                <input type="text" name="login" value="1" hidden>
                <input type="text" name="username" class="form-control email-input" placeholder="Username">
                <div class= "row">
                    <input type="password" name="password" class="form-control password-input" placeholder="Password">
                    <input type="submit" class="btn btn-default">
                </div>
              </form>
          </div>
          <div class ="col-lg-4 col-lg-offset-1 sign-up" >
              <h4> New to Wabl? </h4>
              {if $signup_error}
                <br><h4 style="color: red">{$signup_error}</h4>
              {/if}
              <form method="POST" enctype="multipart/form-data">
                <div class="sign-up-form">
                      <input type="text" name="signup" value="1" hidden>
                      <input type="text" name="realname" class="form-control" placeholder="Name">
                      <input type="text" name="username" class="form-control" placeholder="Username">
                      <input type="password" name="password" class="form-control" placeholder="Password">
                      <input type="password" name="passwordConfirmation" class="form-control" placeholder="Confirm Password">
                </div>
                <button type="signUp" class="btn btn-default sign-up-botton">Sign Up</button>
              </form>
          </div>
      </div>
  </div>

{include file='common/footer.tpl'}
