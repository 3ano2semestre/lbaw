var $window = $(window);
var $document = $(document);
var options = options || [];

$document.ready(function() {

    // Tabs

    var tabPlaceholders = $('.js-placeholder');
    var tabs = $('#page-tabs-nav li:not(.disabled)');

    tabs.on('click', function() {
        tabs.removeClass('active');
        $(this).addClass('active');

        var newTab = tabPlaceholders.filter('.' + $(this).data('content'));
        tabPlaceholders.fadeOut(100);
        newTab.delay(100).fadeIn(200);

        tabPlaceholders.removeClass('active');
        newTab.addClass('active');
    });

    // Infinite Scroll

    var loadmoreButton = $('.js-loadmore');
    loadmoreButton.on('click', function() {
        loadMore();
    });

    var loadMore = function() {
        var placeholder = $('.js-placeholder.active');
        var button = placeholder.find('button.js-loadmore');
        var offset = placeholder.children().length - 1;
        var type = placeholder.data('type');

        button.html('Loading');

        $.ajax({
            type: 'get',
            data: {offset: offset, type: type},
            success: function(data) {
                if (data.length > 0) {
                    $(data).insertBefore(button);
                    button.html('Load More');
                } else {
                    button.hide();
                }
            }
        });
    }

    if (options.indexOf('loadMore') != -1) {
        $window.scroll(function() {
            if($window.scrollTop() + $window.height() + 100 < $document.height()) {
                return;
            }
            loadMore();
        });
    }


    // Votes

    var votes = function() {
        var active = true;
        var info = $('.js-vote-count');
        var buttons = $('.js-vote');

        buttons.on('click', function() {
            if (!active) {
                return;
            }

            var clickedButton = $(this);
            var glyphicon = clickedButton.children('i');

            active = false;
            glyphicon.addClass('spin');
            buttons.removeClass('active');

            var value = clickedButton.data('value');
            $.ajax({
                type: 'get',
                data: {type: 'vote', value: value},
                success: function(data) {
                    if (data.success) {
                        info.html(data.count);
                        clickedButton.addClass('active');
                    }
                    active = true;
                    glyphicon.removeClass('spin');
                }
            });
        });
    };

    votes();

     //Follow
    var follow = $('.js-follow');
    follow.on('click',function()
    {
        var button = $(this);
        var followID = button.attr('followID');
        var type = button.html().trim();

        if (type == 'Loading') {
            return;
        }

        button.html('Loading');

        $.ajax({
            type: 'POST',
            url: '/user/'+followID+'/follow',
            data: {type: type},
            success: function(data) {
                var obj = $.parseJSON(data);
                if (obj.success) {
                    if (type == 'Follow') {
                        button.html('Unfollow');
                    } else {
                        button.html('Follow');
                    }
                } else {
                    alert("Error following user:\n"+obj.about);
                    button.html(type);
                }
            }
        });
    });

     //Subscribe
    var subscribe = $('.js-subscribe');
    subscribe.on('click',function()
    {
        var button = $(this);
        var followID = button.attr('followID');
        var type = button.html().trim();

        if (type == 'Loading') {
            return;
        }

        button.html('Loading');

        $.ajax({
            type: 'POST',
            url: '/collection/'+followID+'/subscribe',
            data: {type: type},
            success: function(data) {
                var obj = $.parseJSON(data);
                if (obj.success) {
                    if (type == 'Subscribe') {
                        button.html('Unsubscribe');
                    } else {
                        button.html('Subscribe');
                    }
                } else {
                    alert("Error following user:\n"+obj.about);
                    button.html(type);
                }
            }
        });
    });



});
