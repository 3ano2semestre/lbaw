$(document).ready(function(){
    $("[contenteditable=true]").blur(function(){
        var field = $(this).attr("id") ;
        var value = $(this).text() ;
        $.ajax(updateArticleUrl, {
            type : 'POST',
            data : {
                field : field,
                value : value
            },
            success : function(data) {
            }
        });
    });

    $("[data-toggle='collection-unsubscribe']").click(function(){

        var collectionId = $(this).attr("target");
        var element = $(this);

        $.ajax(addToCollection, {
            type : 'POST',
            data : {
                collectionId : collectionId
            },
            success : function(data){
                if (data == 1) {
                    element.replaceWith('<button id="collection-subscribe" data-toggle="collection-subscribe"  target="' + collectionId + '" type="button" class="btn btn-success glyphicon glyphicon-thumbs-up collection-subscribe"></button>');
                }
            }

        });

    });

    $("[data-toggle='collection-subscribe']").click(function(){

        var collectionId = $(this).attr("target");
        var element = $(this);

        $.ajax(removeFromCollection, {
            type : 'POST',
            data : {
                collectionId : collectionId
            },
            success : function(data){
                if (data == 0) {
                    element.replaceWith('<button id="collection-unsubscribe" data-toggle="collection-unsubscribe"  target="' + collectionId + '" type="button" class="btn btn-default glyphicon glyphicon-plus-sign collection-unsubscribe"></button>');
                }
            }
        });

    });

    $("[data-toggle='confirmation']").popConfirm();

    var editingContent = false;
    $(".toTinyMCE").click(function(e) {
        editingContent = true;
        var value = $(this).text();
        var $this = $(this);
        $(this).html('<textarea class="search-input story" rows="20" cols="50" name="content" id="content-ta" placeholder="Write your story">' + value + '</textarea>');

        tinymce.init({
            selector:'textarea',
            browser_spellcheck : true,
            toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            menubar: false,
            statusbar: false,
            setup : function(ed) {
                ed.on('blur', function(e) {
                    var content = tinymce.get('content-ta').getContent();
                    $this.html(content);
                    $.ajax(updateArticleUrl, {
                        type : 'POST',
                        data : {
                            field : "content-area",
                            value : content
                        },
                        success : function(data) {
                        }
                    });
                });
            }
        });

    });

});
/*
tinymce.init({
    selector:'textarea',
    browser_spellcheck : true,
    toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
    menubar: false,
    statusbar: false
});*/


$(document).ready(function(){
   $('#newCommentBtn').click(function(e) {
       e.preventDefault();
       var comment = $('#newCommentBox').val();
       if(comment == '') {
           $('#newCommentBox').css('border', '1px solid red');
           $('#newCommentBox').attr('placeholder', 'You must type a comment first!');
       } else {
           $.ajax(addCommentURL, {
               type : 'POST',
               data : {
                   text : comment
               },
               success : function(data){
                   result = JSON.parse(data);
                   if(result.error == false) {
                       $('#newCommentBox').val('');

                        var date = parseInt(Date.now() / 1000);
                       $('#commentsBox').prepend(
                       '<li id="comment-' + result.comment_id + '">'
                          + '<div class="commenterImage">'
                          +     '<img src="' + userPicture + '" />'
                          + '</div>'
                          + '<div class="commentText">'
                          +     '<p class="">' + comment + '</p> <span class="date sub-text" data-livestamp="' + date + '"></span>'
                              + '<span class="sub-text"> by ' + username + '</span>'

                           + '<a href="#" class="deleteComment" target="' + result.comment_id + '" id="href-delete-' + result.comment_id + '">'
                           + ' <span class="sub-text">delete</span>'
                           + '</a>'
                           + '<span id="comment-confirmation-' + result.comment_id + '" style="display:none">'
                           + ' <span class="sub-text">are you sure?</span>'
                           + ' <a href="#" class="confirmDeleteComment" target="' + result.comment_id + '" id="confirm-delete-' + result.comment_id + '">'
                           + '     <span class="sub-text">yes</span>'
                           + ' </a>'
                           + ' <a href="#" class="cancelDeleteComment" target="' + result.comment_id + '" id="cancel-delete-' + result.comment_id + '">'
                           + '     <span class="sub-text">no</span>'
                           + ' </a>'
                           + '</span>'
                          + '</div>'
                       + '</li>');
                       $('#href-delete-' + result.comment_id).click(deleteComment);
                       $('#confirm-delete-' + result.comment_id).click(confirmDeleteComment);
                       $('#cancel-delete-' + result.comment_id).click(cancelDeleteComment);


                   }

               }
           });
       }

   });
   $('#newCommentBox').keydown(function(e) {
       if($(this).val() != '')
            $('#newCommentBox').css('border', '1px solid green');
       else
           $('#newCommentBox').css('border', '1px solid gray');
   });

    // href click
    $('.replyComment').click(function(e) {
        $(this).css('display', 'none');
        e.preventDefault();
        var target = $(this).attr('target');
        $('#form-reply-' + target).css('display','block');
        $( '#input-reply-' + target).focus();
    });

    // reply submit
    $('.newReplyBtn').click(function(e) {
        e.preventDefault();
        var target = $(this).attr('target');
        var reply = $('#input-reply-' + target).val();

        if(reply == '') {
            $('#input-reply-' + target).css('border', '1px solid red');
            $('#input-reply-' + target).attr('placeholder', 'You must type a reply first!');
        } else {
            $.ajax(addCommentURL, {
                type : 'POST',
                data : {
                    text : reply,
                    parent : target
                },
                success : function(data){
                    result = JSON.parse(data);
                    var date = parseInt(Date.now() / 1000);
                    if(result.error == false) {
                        $('#form-reply-' + target).css('display', 'none');
                        $('#input-reply-' + target).val('');
                        $('#href-reply-' + target).css('display','-webkit-inline-box');
                        $('#replies-' + target).append(
                        '<li id="comment-' + result.comment_id + '">'
                           + '<div class="commenterImage">'
                           +  '<img src="' + userPicture + '" />'
                           + '</div>'
                           + '<div class="commentText">'
                           + '<p class="">' + reply + '</p>'
                           + '<span class="date sub-text" data-livestamp="' + date + '"></span>'
                            + '<span class="sub-text"> by ' + username + '</span>'
                           + '<a href="#" class="deleteComment" target="' + result.comment_id + '" id="href-delete-' + result.comment_id + '">' // TODO ID
                           + '   <span class="sub-text">delete</span>'
                           + '</a>'
                           + '<span id="comment-confirmation-' + result.comment_id + '" style="display:none">'
                            + '     <span class="sub-text">are you sure?</span>'
                            + '     <a href="#" class="confirmDeleteComment" target="' + result.comment_id + '" id="confirm-delete-' + result.comment_id + '">'
                            + '     <span class="sub-text">yes</span>'
                            + ' </a>'
                            + ' <a href="#" class="cancelDeleteComment" target="' + result.comment_id + '" id="cancel-delete-' + result.comment_id + '">'
                            + '     <span class="sub-text">no</span>'
                            + ' </a>'
                            + '</span>'
                        +'</div>'
                        + '</li>');

                        $('#href-delete-' + result.comment_id).click(deleteComment);
                        $('#confirm-delete-' + result.comment_id).click(confirmDeleteComment);
                        $('#cancel-delete-' + result.comment_id).click(cancelDeleteComment);

                    }
                }
            });
        }

    });

    var cancelReply = function(e) {
        e.preventDefault();
        var target = $(this).attr('target');
        $('#form-reply-' + target).css('display','none');
        $('#href-reply-' + target).css('display','-webkit-inline-box');
    }

    var deleteComment = function(e) {
        e.preventDefault();
        $(this).css('display', 'none');
        var target = $(this).attr('target');
        $('#comment-confirmation-' + target).css('display','-webkit-inline-box');
    }

    var cancelDeleteComment = function(e) {
        e.preventDefault();
        var target = $(this).attr('target');
        $('#comment-confirmation-' + target).css('display','none');
        $('#href-delete-' + target).css('display','-webkit-inline-box');
    }

    var confirmDeleteComment = function(e) {
        e.preventDefault();
        var target = $(this).attr('target');
        $('#comment-confirmation-' + target).css('display','none');
        $('#comment-' + target).remove();
        $.ajax(deleteCommentURL, {
            type : 'DELETE',
            data : {
                commentId : target
            },
            success : function(data){
            }
        });
    }
    // reply cancel
    $('.cancelReply').click(cancelReply);

    $('.deleteComment').click(deleteComment);

    $('.cancelDeleteComment').click(cancelDeleteComment);

    $('.confirmDeleteComment').click(confirmDeleteComment);


    var voteComment = function(e) {
        e.preventDefault();
        var span = $(":first-child", $(this));
        var target = $(this).attr('target');
        var value = $(this).attr('value');
        $.ajax(voteCommentURL, {
            type : 'POST',
            data : {
                commentId : target,
                value     : value
            },
            success : function(data){
                if(value == 1) {
                    span.css('color','green');
                    var upValue = parseInt($('#span-voteup-' + target).html()) + 1;
                    $('#span-voteup-' + target).html("+" + upValue);
                    if($('#span-votedown-' + target).css('color') == 'rgb(255, 0, 0)') {
                        var downValue = parseInt($('#span-votedown-' + target).html()) + 1;
                        //alert(downValue);
                        $('#span-votedown-' + target).html("-" + downValue);
                    }
                    $('#span-votedown-' + target).css('color','gray');
                }
                else if(value == -1) {
                    span.css('color', 'red');
                    var downValue = parseInt($('#span-votedown-' + target).html()) + 1;
                    $('#span-votedown-' + target).html("-" + downValue);
                    if($('#span-voteup-' + target).css('color') == 'rgb(0, 128, 0)') {
                        var upValue = parseInt($('#span-voteup-' + target).html()) - 1;
                        //alert(downValue);
                        $('#span-voteup-' + target).html("+" + upValue);
                    }
                    $('#span-voteup-' + target).css('color','gray');
                }

            }
        });
    }

    $('.voteComment').click(voteComment);

});

