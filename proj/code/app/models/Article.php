<?php

class Article extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'articles';

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function collections()
    {
        return $this->belongsToMany('Collection', 'collection_article', 'article_id', 'collection_id');
    }

    public function votes()
    {
        return $this->hasMany('ArticleVote');
    }

}
