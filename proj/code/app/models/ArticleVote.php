<?php

class ArticleVote extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'article-votes';

    public function article()
    {
        return $this->belongsTo('Article');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

}
