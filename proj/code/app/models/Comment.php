<?php
/**
 * Created by IntelliJ IDEA.
 * User: Emanuelpinho
 * Date: 16/05/14
 * Time: 16:46
 */

class Comment extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

    public function article()
    {
        return $this->belongsTo('Article');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }


    public function votes()
    {
        return $this->hasMany('CommentVote');
    }

    public function parent() {
        return $this->belongsTo('Comment');
    }

    public function childComments() {
        return $this->hasMany('Comment', 'parent_id');
    }

    public function getPlusVotes() {
        return $this->votes()->where('value','=',1)->count();
    }
    public function getMinusVotes() {
        return $this->votes()->where('value','=',-1)->count();
    }

}
