<?php
/**
 * Created by IntelliJ IDEA.
 * User: pedro
 * Date: 08/06/14
 * Time: 23:22
 */

class Error extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'errors';

} 