<?php
/**
 * Created by IntelliJ IDEA.
 * User: Emanuelpinho
 * Date: 16/05/14
 * Time: 16:48
 */

class Follower  extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'followers';

}
