<?php
/**
 * Created by IntelliJ IDEA.
 * User: Emanuelpinho
 * Date: 16/05/14
 * Time: 16:46
 */

class Collection extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'collections';

    public function articles()
    {
        return $this->belongsToMany('Article', 'collection_article');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function followers()
    {
      return $this->belongsToMany('User', 'subscriptions', 'collection_id', 'user_id');
    }
}
