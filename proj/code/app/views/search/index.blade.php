@extends('common.master')

@section('content')

<script>
    var setEnabledURL = '{{URL::route('setEnabledUser')}}';
    var setPrivilegeURL = '{{URL::route('setUserPrivilege')}}';
</script>

<div class="container">
    <ul id="page-tabs-nav" style="margin-top: 17px">
        <li style="cursor:pointer" class="active" data-content="articles">Articles</li>
        <li style="cursor:pointer" data-content="collections">Collections</li>
        <li style="cursor:pointer" data-content="users">Users</li>
    </ul>
</div>
<div class="container">
    <ul class="timeline js-placeholder articles active" data-type="articles">
        @foreach ($articles as $article)
            @include ('common.article', array('article'=>$article))
        @endforeach
        @if (!count($articles))
        <div class="well">
            No articles found.
        </div>
        @else
        @include ('common.loadmore-button')
        @endif
    </ul>
    <ul class="timeline js-placeholder collections" data-type="collections" hidden>
        @foreach ($collections as $collection)
            @include ('common.collection', array('collection'=>$collection))
        @endforeach
        @if (!count($collections))
        <div class="well">
            No collections found
        </div>
        @else
        @include ('common.loadmore-button')
        @endif
    </ul>
    <ul class="timeline js-placeholder users" data-type="users" hidden>
        @foreach ($users as $user)
            @include ('common.user', array('users'=>$user, 'privilege'=>$privilege))
        @endforeach
        @if (!count($users))
        <div class="well">
            No users found
        </div>
        @else
        @include ('common.loadmore-button')
        @endif
    </ul>


</div>

@include ('common.activate-loadmore')

@stop
