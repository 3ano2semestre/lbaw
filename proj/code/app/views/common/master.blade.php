<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico" />

    <title>WABL</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('css/bootstrap.min.css'); }}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
    </script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js">
    </script>
    <![endif]-->

    <!-- Custom styles for this template -->
    {{ HTML::style('css/carousel.css'); }}
    {{ HTML::style('css/style.css'); }}
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
</head>

<body>

    <!-- Carousel -->
    @if (isset($showCarousel) and $showCarousel)
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
          <div class="item active">
            <div class="container">
              <div class="carousel-caption">
                <h1>{wabL}</h1>

                <p>Plataforma de partilha de notícias e artigos</p>

                <p><a class="btn btn-lg btn-primary" href="{{ URL::route('login') }}" role="button">Sign Up</a></p>
              </div>
            </div>
          </div>
      </div>
    </div>
    <!-- /.carousel -->
    @endif

    @include('common.header')

    @yield('content')

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->


    {{ HTML::script('js/bootstrap.min.js') }}
    

    {{ HTML::script('js/main.js') }}
    {{ HTML::script('js/article.js') }}
    {{ HTML::script('js/search.js') }}
    {{ HTML::script('js/tinymce/tinymce.min.js') }}
    {{ HTML::script('js/moment.min.js') }}
    {{ HTML::script('js/livestamp.min.js') }}
    {{ HTML::script('js/jquery.popconfirm.js') }}
    

    <script src="/js/profilePageHandler.js"></script>
    <script src="/js/settingsHandler.js"></script>

</body>
</html>
