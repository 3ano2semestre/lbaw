<?php
if(!isset($privilege))
    $privilege = 1;
?>
<li>
  <div class="timeline-badge" style="background-image: url({{ $user->getGravatarUrl() }})"></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
    @if(Auth::check() && Auth::user()->id != $user->id)
      <button type="button" class="btn btn-default js-follow" id="followButton" followID="{{$user->id}}">
      {{ Auth::user()->follows($user) ? 'Unfollow' : 'Follow' }}
      </button>
    @endif
      <a href="{{ URL::to('user/'.$user->id)}}"><h4 class="timeline-title">{{$user->username}}</h4></a>
      <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> <span data-livestamp="{{$user->created_at}}"></span></small></p>
      @if($privilege > 1)
        @if($user->active == 't')
            <a href="#" class="enable-disable-user" action="disable" user-target="{{$user->id}}"><small>Disable user</small></a>
        @else
            <a href="#" class="enable-disable-user" action="enable" user-target="{{$user->id}}"><small>Enable user</small></a>
        @endif
        @if($privilege > 2)
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle privilege-dropdown" id="privilege-{{$user->id}}" data-toggle="dropdown">
                {{$user->getUserPermissionName()}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" user-id="{{$user->id}}" privilege="3">Admin</a></li>
                <li><a href="#" user-id="{{$user->id}}" privilege="2">Moderator</a></li>
                <li><a href="#" user-id="{{$user->id}}" privilege="1">User</a></li>
            </ul>
        </div>
        @endif
      @endif
    </div>
    <div class="timeline-body">
      <p>{{$user->description}}</p>
    </div>
  </div>
</li>
