<li>
  <div class="timeline-badge" style="background-image: url({{ $collection->user()->first()->getGravatarUrl() }})"></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
     @if(Auth::check() && Auth::user()->id != $collection->user_id)
        <button type="button" class="btn btn-default js-subscribe" followID="{{$collection->id}}" style="float: right">
          {{ (Auth::user()->isSubscribedToCollection($collection)) ? 'Unsubscribe' : 'Subscribe' }}
        </button>
      @endif
      <a href="{{ URL::To('collection/'.$collection->id)}}"><h4 class="timeline-title">{{$collection->name}}</h4></a>
      <p><small class="text-muted" data-livestamp="1401986627"><i class="glyphicon glyphicon-time"></i> <span data-livestamp="{{$collection->created_at}}"></span></small></p>
    </div>
    <div class="timeline-body">
      <p><!--{{$collection->subtitle}}--></p>
    </div>
  </div>
</li>
