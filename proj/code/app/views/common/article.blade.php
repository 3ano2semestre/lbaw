<li>
  <div class="timeline-badge" style="background-image: url({{ $article->user()->first()->getGravatarUrl() }})"></div>
  <div class="timeline-panel">
    <div class="timeline-heading">
      <a href="{{ URL::to('article/'.$article->id)}}"><h4 class="timeline-title">{{$article->title}}</h4></a>
      <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> <span data-livestamp="{{$article->created_at}}"></span></small></p>
    </div>
    <div class="timeline-body">
      <p>{{$article->subtitle}}</p>
    </div>
  </div>
</li>
