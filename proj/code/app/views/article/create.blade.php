@extends('common.master')

@section('content')

<div class="container">
    <form action="#" method="POST" enctype="multipart/form-data">
        <div class="col-xs-12 article create-article">
            <input name="title" class="search-input title" type="text" placeholder="Title" />
            <p class="help-block">Insert here the title of the article. (Required) </p>
            <input name="sub" class="search-input sub-title" type="text" placeholder="Sub-title" />
            <p class="help-block">Insert here the sub-title of the article. (Required)</p>
            {{HTML::image("img/add_image.png",'add-image', array("class"=>"add-image"))}}
            <input type="file" name="image" id="image" >
            <p class="help-block">Upload a background image of the article. </p>
            <textarea class="search-input story" rows="4" cols="50" name="content" placeholder="Write your story"></textarea>
        </div>
        <p class="help-block">Write here the content of the article. (Required)</p>
        <div class="col-xs-12 article">
            <input type="submit" value="Submeter" />
            <input type="checkbox" name="private-article" value="private" id="private-article" />
            <label for="private-article">Privado</label>
        </div>
    </form>
</div>

@stop