@extends('common.master')

@section('content')
<div class = "container">
	<div class = "errorDiv">
		<h1 style="text-align: center;">
			Error 404!
		<br>	
			Page not found
			<img src="{{asset('img/404.jpg')}}" height="372" width="361">
		</h1>
	</div>
<div>
@stop