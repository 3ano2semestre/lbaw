@extends('common.master')

@section('content')
<div class="container">
    <form action="#" method="POST" enctype="multipart/form-data">
        <div class="col-xs-12 article create-article">
            <input name="name" class="search-input title" type="text" placeholder="Name" value="{{{$collection->name}}}" />
            <input name="description" class="search-input sub-title" type="text" placeholder="Description" value="{{{$collection->description}}}" />
            {{HTML::image("img/add_image.png",'add-image', array("class"=>"add-image"))}}
            <input type="file" name="image" id="image" >
        </div>
        <div class="col-xs-12 article">
            <input type="submit" value="Save" />
            <input type="checkbox" name="private-article" value="private" id="private-article" />
            <label for="private-article">Privado</label>
        </div>
    </form>
</div>
@stop