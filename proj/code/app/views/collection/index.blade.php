@extends('common.master')

@section('content')

<!-- Profile Info -->
<div class="container perfil-page info">
	@if (is_null($collection->imageurl))
    <div class="row cover"></div>
    @else
    <div class="row cover" style="background: url(' {{$collection->imageurl;}}')center center no-repeat fixed;"></div>
    @endif
    <div class="row description">
        <h2>{{$collection->name}}</h2>
        <p class="lead"> <small> {{$collection->description}} </small> </p>
            <div class="row">
                @if (isset($editable) && $editable)
                <a data-toggle="confirmation" data-placement="right" data-original-title="" title="" href="{{ URL::to('collection/delete/' . $collection->id) }}">
                    <button type="button" id="remove-article-button" class="btn btn-primary add-to-collection">
                        Remove
                    </button>
                </a>
                @endif
                @if(Auth::check() && Auth::user()->id != $collection->user_id)
                    <button type="button" class="btn btn-default js-subscribe" followID="{{$collection->id}}">
                      {{ (Auth::user()->isSubscribedToCollection($collection)) ? 'Unsubscribe' : 'Subscribe' }}
                    </button>
                @endif
            </div>
        @if (isset($editable) && $editable)
            <a href="{{URL::route('editCollection',array('id'=>$collection->id))}}">
                <p class="lead"> <small> Edit </small> </p>
            </a>
        @endif

    </div>
  </div>

<!-- Feed -->
<div class="container">

    <ul class="timeline js-placeholder active" data-type="articles">
      	@foreach ($collection_articles as $article)
			@include ('common.article', array('article'=>$article))
	    @endforeach
        @if (!count($collection_articles))
        <div class="well">
            No published stories
        </div>
        @else
        @include ('common.loadmore-button')
        @endif
    </ul>
</div>

@include ('common.activate-loadmore')

@stop
