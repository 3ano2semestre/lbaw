@extends('common.master')

@section('content')

<script>
    var updateURL = '{{URL::route('updateUser')}}';
    var userId = {{$user->id}};
</script>

<div class="container">
    <div class="col-xs-12 article">
    </div>
        <div class="col-xs-12 settings">
            <h1>Settings</h1>
            <div class="row setting-option">
                <h4>Delete Account</h4>
                <a data-toggle="confirmation" data-placement="right" data-original-title="" title="" href="{{URL::route('removeUser', array('id'=>$user->id))}}">
                    <button type="button" id="remove-article-button" class="btn btn-danger add-to-collection">
                        Remove
                    </button>
                </a>
            </div>
            <div class="row setting-option">
                <h4>Change Email</h4>
                <br><br><input type="email" class="form-control" id="InputEmail" placeholder="Enter email"><br>
                <button type="button" class="btn btn-default" id="settingsEmail">Change Email</button>
            </div>
            <div class="row setting-option">
            	<h4>Replace Password</h4>
            	<br><br>
            	<form>
            		Original password: <input type="password" class="form-control" id="InputOriginalPassword" placeholder="Original Password"><br>
					New password: <input type="password" class="form-control" id="InputNewPassword" placeholder="New Password"><br>
					Confirm password: <input type="password" class="form-control" id="InputConfirmPassword" placeholder="Confirm New Password">
				</form>
				<br>
                <button type="button" class="btn btn-default" id="settingsPassword">Change password</button>
            </div>
			@if ($user->privilege > 1)
            <div class="row setting-option">
                <h4>Moderator</h4>
                <button type="button" class="btn btn-default">PROMOTE</button>
                <button type="button" class="btn btn-default">DEMOTE</button>
                <input class="search-input moderator-search" type="text" placeholder="User">
                <input class="search-input moderator-search" type="text" placeholder="Collection">
            </div>
            @endif
        </div>

  </div>

@stop