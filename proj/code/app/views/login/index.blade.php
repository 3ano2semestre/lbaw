@extends('common.master')

@section('content')

<div class="container login-container">
    <div class="row">
        <div class ="col-lg-7 sign-in">
            <h3> Welcome to Wabl </h3>
            <!--  <div class="row fb-sign">
                <h5> Sign in here </h5> or <img src="img/fblogin.png" width="250" />
            </div> -->
            @if($error)
            <h4 style="color: red">{{ $error }}</h4>
            @endif

            <form method="POST" enctype="multipart/form-data">
                <input type="text" name="login" value="1" hidden>
                <input type="text" name="username" class="form-control email-input" placeholder="Username">
                <div class= "row">
                    <input type="password" name="password" class="form-control password-input" placeholder="Password">
                    <input type="submit" class="btn btn-default">
                </div>
            </form>
        </div>
        <div class ="col-lg-4 col-lg-offset-1 sign-up" >
            <h4> New to Wabl? </h4>
            @if($registrationError)
            <br><h4 style="color: red">{{ $registrationError }}</h4>
            @endif
            <form method="POST" action="{{ URL::route('register') }}" enctype="multipart/form-data">
                <div class="sign-up-form">
                    <input type="text" name="signup" value="1" hidden>
                    <input type="email" name="email" class="form-control" value="{{ Input::old('email') }}" placeholder="Email" required>
                    <input type="text" name="username" class="form-control" value="{{ Input::old('username') }}" placeholder="Username" required>
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required>
                </div>
                <button type="signUp" class="btn btn-default sign-up-botton">Sign Up</button>
            </form>
        </div>
    </div>
</div>

@stop
