@extends('common.master')

@section('content')
<div class="container">
    <ul id="page-tabs-nav" style="margin-top: 17px">
        @if (isset($feed))
        <li style="cursor:pointer" class="active" data-content="feed">Your Feed</li>
        <li style="cursor:pointer" data-content="latest">Latest</li>
        @else
        <li style="cursor:pointer" class="active" data-content="latest">Latest</li>
        @endif
    </ul>
</div>
<div class="container">
    @if (isset($feed))
    <ul class="timeline js-placeholder feed active" data-type="feed">
        @foreach ($feed as $article)
            @include ('common.article', array('article' => $article))
        @endforeach
        @if (!count($feed))
        <div class="well">
            People you follow haven't published any stories
        </div>
        @else
        @include ('common.loadmore-button')
        @endif
    </ul>
    @endif

    <ul data-type="latest" class="timeline js-placeholder latest @if (!isset($feed)) active @endif"
        @if (isset($feed)) hidden @endif>
        @foreach ($articles as $article)
            @include ('common.article', array('article' => $article))
        @endforeach
        @if (!count($articles))
        <div class="well">
            No published stories
        </div>
        @else
        @include ('common.loadmore-button')
        @endif
    </ul>
</div>

@include ('common.activate-loadmore')

@stop
