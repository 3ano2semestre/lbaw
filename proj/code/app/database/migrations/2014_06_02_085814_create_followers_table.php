<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('followers', function($table)
        {
            $table->integer('follower_id')->unsigned();
            $table->integer('followee_id')->unsigned();

            $table->foreign('follower_id')->references('id')->on('users');
            $table->foreign('followee_id')->references('id')->on('users');

            $table->primary(array('followee_id', 'follower_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('followers');
    }
}
