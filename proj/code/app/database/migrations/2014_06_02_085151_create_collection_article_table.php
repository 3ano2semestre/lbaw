<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionArticleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('collection_article', function($table)
        {
            $table->integer('collection_id')->unsigned();
            $table->integer('article_id')->unsigned();

            $table->foreign('collection_id')->references('id')->on('collections');
            $table->foreign('article_id')->references('id')->on('articles');

            $table->primary(array('collection_id', 'article_id'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('collection_article');
	}

}
