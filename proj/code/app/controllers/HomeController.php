<?php

class HomeController extends BaseController {

	public function index()
	{

		if (Request::ajax()) {
			return $this->getContent();
		}

		if ($this->user) {
			$feed = $this->getFeed();
			View::share('feed', $feed);

		} else {
			View::share('showCarousel', true);
		}

		$articles = $this->getLatest();
		View::share('articles', $articles);

        return View::make('home/index');
	}

	protected function getContent()
	{
		$offset = Input::get('offset', 0);
		$type = Input::get('type', null);

		$response = "";
		$objects = [];

		if ($type == 'feed') {
			$objects = $this->getFeed($offset);
		} else if ($type == 'latest') {
			$objects = $this->getLatest($offset);
		}

		foreach ($objects as $object) {
			$response .= (string)View::make('common/article', array('article' => $object));
		}

		return Response::json($response);
	}

	protected function getFeed($offset = 0)
	{
		// Follows
		$follows = $this->user->followees()->get();
		$followIds = [];

		foreach ($follows as $follow) {
			$followIds[] = $follow->id;
		}

		// Collections
		//$collections = (array)$this->user()->collections()->select('id')->get();
        $collections = $this->user->subscriptions()->get();

        $queries = DB::getQueryLog();
        $last_query = end($queries);

        $collectionArticles = [];
        foreach($collections as $collection) {
            //die(json_encode($collection->id));
            $articles = $collection->articles()->get();
            foreach($articles as $a) {
                $collectionArticles[] = $a->id;
            }
        }

        if(count($followIds) == 0 && count($collectionArticles) == 0)
            return array();

        if(count($followIds) > 0) {
            $q = Article::whereIn('user_id', $followIds);
        }
        if(count($collectionArticles) > 0) {
            if(isset($q)) {

                $q = $q->orWhereIn('id',$collectionArticles);
            }
            else {
                $q = Article::whereIn('id',$collectionArticles);
            }
        }
		return  $q->orderBy('created_at', 'desc')
			->groupBy('id')
			->skip($offset)
			->take(10)
			->get();
	}

	protected function getLatest($offset = 0)
	{
		return Article::take(10)
			->orderBy('created_at', 'desc')
			->skip($offset)
			->get();
	}

}
