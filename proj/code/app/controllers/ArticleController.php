<?php

	class ArticleController extends BaseController {

    protected $layout = 'common.master';

	public function index($id)
	{
        $article = Article::find($id);

        if(!$article || $article->active != 't') {
            App::abort(404);
        }

        $type = Input::get('type', null);
        if (Request::ajax() && $type == 'vote') {
            return $this->vote($article);
        }

		$articleUser = $article->user;
        $comments = $article->comments()->where('parent_id','=',NULL)->orderBy('created_at','desc')->get();


        //die(json_encode($comments[0]->votes()->where('value','=',1)->count()) . " -");

		$view = View::make('article.index');

		$view->article = $article;
		$view->articleUser = $articleUser;
        $view->comments= $comments;
        $view->user = null;
        $articleCollections = $article->collections;

        $votes = array();

        $view->editable = false;
        if(Auth::check()) {
            $view->user = Auth::user();

            $allVotes = $view->user->commentVotes()->get();

            foreach($allVotes as $v) {
                $votes[$v->comment_id] = $v->value;
            }

            if($view->user->id == $articleUser->id) {
                $collections = Collection::where('user_id', '=' ,$articleUser->id)->get();
                $articlesInCollection = array();

                foreach($collections as $c) {
                    foreach($articleCollections as $ac) {
                        if ($ac['id'] == $c['id']) {
                            $articlesInCollection[] = $ac['id'];
                        }
                    }
                }
                $view->articlesInCollection = $articlesInCollection;
                $view->collections = $collections;
                $view->editable = true;
            }
        }

        // Check if already voted
        $view->voted = 0;
        if ($this->user) {
            $vote = ArticleVote::where('user_id','=', $this->user->id)->where('article_id','=',$article->id)->get();
            if (count($vote) > 0) {
                $view->voted = $vote[0]->value;
            } else {
                $view->voted = 0;
            }
        }

        //die(json_encode($votes));
        $view->votes = $votes;
        $view->voteCount = $article->votes()->sum('value');
        $this->layout->content = $view;
	}

    public function create()
    {
        $view = View::make('article.create');
        $this->layout->content = $view;
    }

    public function insert()
    {
        if(!Input::has('title') || !Input::has('sub') || !Input::has('content')) {

            return Redirect::route('createArticle');
        }

        $title = htmlspecialchars(Input::get('title'));
        $sub = htmlspecialchars(Input::get('sub'));
        $content = Input::get('content');
        $user_id = Auth::user()->id;

        $article = new Article();
        $article->title = $title;
        $article->user_id = $user_id;
        $article->subtitle = $sub;
        $article->content = $content;

        $article->save();

        if (Input::hasFile('image')) {
            $path = 'img/article/' . $article->id;
            File::makeDirectory($path, $mode = 755, true, true);
            $dt = new DateTime();
            $extension = Input::file('image')->getClientOriginalExtension();
            $fileName = $dt->getTimestamp() . '-' . $user_id . '.' . $extension ;
            Input::file('image')->move($path, $fileName);

            $article->imageurl = $path . '/' . $fileName;
        }

        $article->save();

        return Redirect::route('article', array('id'=>$article->id));
    }

	public function delete($id)
	{
        $user_id = Auth::user()->id;
        $article = Article::find($id);

        if ($article == null)
            App::abort(404);

        if ($article['user_id'] != $user_id){
            App::abort(501);
        }

        $article->collections()->detach();
        $article->comments()->delete();

        $votes = $article->votes()->get();
        foreach($votes as $v) {
            $v->delete();
        }
        
        $article->delete();

        return Redirect::route('home');
	}

	public function addComment($id)
	{
        $result = array('error' => false);
        if(!Input::has('text')) {
            $result['error'] = true;
            $result['message'] = 'No input "text" sent';
        }

        $article = Article::find($id);
        if(!$article) {
            $result['error'] = true;
            $result['message'] = 'Article not found';
        }

        if(!Auth::check()) {
            $result['error'] = true;
            $result['message'] = 'Not logged in';
        }

        if($result['error'])
            return json_encode($result);

        $user = Auth::user();

        $comment = new Comment();

        $comment->user()->associate($user);
        $comment->article()->associate($article);
        $comment->content = Input::get('text');

        if(Input::has('parent')) {
            $parent = Comment::find(Input::get('parent'));
            $comment->parent()->associate($parent);
        }

        $comment->save();

        $result['comment_id'] = $comment->id;
        return json_encode($result);
		//addComment needs article id + parent comment id
	}

	public function removeComment($articleId)
	{
        $result = array('error' => false);

        if(!Input::has('commentId')) {
            $result['error'] = true;
            $result['message'] = 'Comment Id not sent';
        }

        $article = Article::find($articleId);
        if(!$article) {
            $result['error'] = true;
            $result['message'] = 'Article not found';
        }
        if($result['error']) {
            return json_encode($result);
        }

        $comment = $article->comments()->where('id','=',Input::get('commentId'))->first();
        if(!$comment) {
            $result['error'] = true;
            $result['message'] = 'Comment not found';
            return json_encode($result);
        }

        $comment->childComments()->delete();
        $comment->delete();

        return json_encode($result);
	}

    public function update($id)
    {
        $result = array('error' => false);
        $article = Article::find($id);

        if(!$article) {
            $result['error'] = true;
            $result['message'] = "Article not found";
            return json_encode($result);
        }

        if(!Input::has('field') || !Input::has('value')) {
            $result['error'] = true;
            $result['message'] = "No valid field";
            return json_encode($result);
        }
        if(Input::get('field') == 'title'){
            $article->title = Input::get('value');
        }
        if(Input::get('field') == 'subtitle'){
            $article->subtitle = Input::get('value');
        }
        if(Input::get('field') == 'content-area'){
            $article->content = Input::get('value');
        }

        $article->save();

        return json_encode($result);
    }

    public function addToCollection($id)
    {
        $article = Article::find($id);

        if(!$article || !Input::has('collectionId')) {
            die("fail");
        }

        $collection = Collection::find(Input::get('collectionId'));

        $data = 1;

        foreach ( $collection->articles as $collections) {
            if ($collections['pivot']['article_id'] == $id ) {
                $data = 0;
            }
        }
        if ( $data == 1) {
            $collection->articles()->attach($id);
        }

        return $data;

    }

    public function removeFromCollection($id)
    {
        $article = Article::find($id);

        if(!$article || !Input::has('collectionId')) {
            die("fail");
        }

        $collection = Collection::find(Input::get('collectionId'));

        $data = 1;

        foreach ( $collection->articles as $collections) {
            if ($collections['pivot']['article_id'] == $id ) {
                $data = 0;
            }
        }

        if ( $data == 0) {
            $collection->articles()->detach($id);
        }

        return $data;
    }

    public function voteComment($id)
    {
        $result = array('error' => false);
        $article = Article::find($id);

        if(!$article) {
            $result['error'] = true;
            $result['message'] = "Article not found";
        }

        if(!Input::has('commentId') || !Input::has('value')) {
            $result['error'] = true;
            $result['message'] = 'Missing parameters';
        }

        $value = Input::get('value');

        if($value != -1 && $value != 1) {
            $result['error'] = true;
            $result['message'] = 'Invalid vote value';
        }
        $comment = $article->comments()->where('id','=',Input::get('commentId'))->first();

        if(!$comment)  {
            $result['error'] = true;
            $result['message'] = "Comment not found";
        }



        if(!Auth::check()) {
            $result['error'] = true;
            $result['message'] = "Not logged in";
        }

        if($result['error'])
            return json_encode($result);


        $user = Auth::user();

        CommentVote::where('user_id','=', $user->id)->where('comment_id','=',$comment->id)->delete(); // delete existing comment if any

        $commentVote = new CommentVote();
        $commentVote->user()->associate($user);
        $commentVote->comment()->associate($comment);
        $commentVote->value = $value;
        $commentVote->save();

        return json_encode($result);
    }

    private function vote($article)
    {
        if (!$this->user) {
            return Response::json(['success' => false]);
        }

        $value = Input::get('value', 1);

        ArticleVote::where('user_id','=', $this->user->id)->where('article_id','=',$article->id)->delete();

        $vote = new ArticleVote();
        $vote->user()->associate($this->user);
        $vote->article()->associate($article);
        $vote->value = $value;
        $vote->save();

        return Response::json(['success' => true, 'registered' => $value, 'count' => $article->votes()->sum('value')]);
    }
}
