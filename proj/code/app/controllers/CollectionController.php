<?php

class CollectionController extends BaseController {

    protected $layout = 'common.master';

    public function index($id)
   	{
   		$collection = Collection::find($id);

        if (!$collection/* || $collection->active != 't'*/) {
            App::abort(404);
        }

        $offset = Input::get('offset', 0);

   		$collection_articles = $this->getArticles($collection, $offset);

        if (Request::ajax()) {
            $response = "";

            foreach ($collection_articles as $value) {
                $response .= (string)View::make('common/article', array('article' => $value));
            }

            return Response::json($response);
        }

		$view = View::make('collection/index');
		$view->collection = $collection;
		$view->collection_articles = $collection_articles;

        if(Auth::check()) {
            if($collection->user_id == Auth::user()->id) {
                $view->editable = true;
            }
        }

		$this->layout->content = $view;
   	}

    protected function getArticles($collection, $offset)
    {
        return $collection->articles()->skip($offset)->take(10)->get();
    }

    public function create() {
        $view = View::make('collection.create');
        $this->layout->content = $view;
    }

    public function insert() {
        if(!Input::has('name') || !Input::has('description')) {
            return Redirect::route('createCollection');
        }
        $user_id = Auth::user()->id;

        $name        = Input::get('name');
        $description = Input::get('description');

        $collection = new Collection();
        $collection->name = $name;
        $collection->description = $description;
        $collection->user_id = $user_id;
        $collection->save();

        if (Input::hasFile('image'))
        {
            $path = 'img/collection/' . $collection->id;
            File::makeDirectory($path, $mode = 644, true, true);
            $dt = new DateTime();
            $extension = Input::file('image')->getClientOriginalExtension();
            $fileName = $dt->getTimestamp() . '-' . $user_id . '.' . $extension ;
            Input::file('image')->move($path, $fileName);

            //$collection->imageurl = $path . '/' . $fileName;
        }
        return Redirect::route('collection', array('id' => $collection->id));
    }

    public function edit($id) {
        if(!Auth::check()) {
            return Redirect::route('collection', array('id' => $id));
        }
        $collection = Collection::find($id);

        // TODO OWNERSHIP
        if($collection->user_id != Auth::user()->id) {
            return Redirect::route('collection', array('id' => $id));
        }

        $view = View::make('collection.edit');
        $view->collection = $collection;
        $this->layout->content = $view;
    }

    public function save($id) {
        if(!Auth::check() || !Input::has('name') || !Input::has('description')) {
            return Redirect::route('collection', array('id' => $id));
        }
        $collection = Collection::find($id);

        // TODO OWNERSHIP
        if($collection->user_id != Auth::user()->id) {
            return Redirect::route('collection', array('id' => $id));
        }

        $collection->name = Input::get('name');
        $collection->description = Input::get('description');

        $collection->save();

        return Redirect::route('collection', array('id' => $collection->id));

    }

    public function delete($id)
    {
        $user_id = Auth::user()->id;
        $collection = Collection::find($id);

        if ($collection == null)
            App::abort(404);

        if ($collection['user_id'] != $user_id){
            die("Not permissions");
        }

        $collection->articles()->detach();
        $collection->delete();

        return Redirect::route('home');

    }

    public function getMoreArticles($id,$numberArticle)
    {
        $collection = Collection::find($id);
        $newArticles = $collection->articles()->skip($numberArticle)->take(10)->get();
        if(sizeof($newArticles)<=0)
            return Response::json([]);
        else
        {
            $response = array();
            foreach($newArticles as $value) {
                $view = View::make('common/article', array('article'=>$value));
                $contents = (string) $view;
                $response[]=$contents;
            }

            return Response::json($response);
        }
    }

    public function subscribe($id)
    {   
        $subscription = Collection::find($id);
        $action = Input::get('type',null);
        
        if($action=="Subscribe")
        {   
            if($this->user->isSubscribedToCollection($subscription)) 
            {
                $result = array('success' => false);
                $result['about']='User already subscribed';
                return json_encode($result);
            }
            else
            {
                $this->user->subscriptions()->attach($id);
                $result = array('success' => true);
                $result['result']='subscripted';
                return json_encode($result);    
            }

        }
        else if($action=="Unsubscribe")
        {
            if($this->user->isSubscribedToCollection($subscription))
            {
                $this->user->subscriptions()->detach($id);
                $result = array('success' => true);
                $result['result']='unsubscripted';
                return json_encode($result);
            }
            else
            {
                $result = array('success' => false);
                $result['about']='Collection was not subscripted';
                return json_encode($result);
            }
        }
        else
        {
            $result = array('success' => false);
            $result['about']='Invalid action';
            return json_encode($result);
        }
    }
}
