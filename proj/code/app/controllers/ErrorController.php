<?php

class ErrorController extends BaseController {

	public function show404()
	{
		return View::make('error/404');
	}

	public function show401()
	{
		return View::make('error/401');
	}
}
