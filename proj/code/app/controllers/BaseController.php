<?php

class BaseController extends Controller {

	protected $layout = 'common.master';

	public function __construct()
    {
        $this->user = Auth::user();
        if ($this->user && $this->user->active != 't') {
			$this->user = null;
		}
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if (!is_null($this->layout)) {
			$this->layout = View::make($this->layout);
		}

        View::share('user', $this->user);
	}
}
