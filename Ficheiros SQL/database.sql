--
-- PostgreSQL tables & triggers
--

DROP TABLE IF EXISTS "Articles" CASCADE;
DROP TABLE IF EXISTS "ArticleVotes" CASCADE;
DROP TABLE IF EXISTS "CollectionArticle" CASCADE;
DROP TABLE IF EXISTS "Collections" CASCADE;
DROP TABLE IF EXISTS "Comments" CASCADE;
DROP TABLE IF EXISTS "CommentVotes" CASCADE;
DROP TABLE IF EXISTS "Followers" CASCADE;
DROP TABLE IF EXISTS "Subscriptions" CASCADE;
DROP TABLE IF EXISTS "Users" CASCADE;
DROP TABLE IF EXISTS "Articles_FTI" CASCADE;


CREATE TABLE "Articles" (
    id SERIAL,
    title CHARACTER VARYING(70) NOT NULL,
    content text NOT NULL,
    created_at DATE,
    updated_at DATE,
    user_id INTEGER NOT NULL,
    active BOOLEAN DEFAULT TRUE NOT NULL,
    imageurl TEXT DEFAULT NULL
);


CREATE TABLE "ArticleVotes" (
    id SERIAL,
    article_id INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    VALUE INTEGER NOT NULL
);

CREATE TABLE "CollectionArticle" (
    collection_id INTEGER NOT NULL,
    article_id INTEGER NOT NULL
);

CREATE TABLE "Collections" (
    id SERIAL,
    name text NOT NULL,
    description CHARACTER VARYING(200) NOT NULL,
    user_id INTEGER NOT NULL,
    created_at DATE,
    updated_at DATE,
    imageurl TEXT DEFAULT NULL

);

CREATE TABLE "Comments" (
    id SERIAL,
    content text NOT NULL,
    parent_id INTEGER,
    article_id INTEGER NOT NULL,
    created_at DATE,
    updated_at DATE,
    user_id INTEGER NOT NULL
);

CREATE TABLE "CommentVotes"(
    id SERIAL,
    value INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    comment_id INTEGER NOT NULL
);

CREATE TABLE "Followers" (
    follower_id INTEGER NOT NULL,
    followee_id INTEGER NOT NULL
);

CREATE TABLE "Subscriptions" (
    user_id INTEGER NOT NULL,
    collection_id INTEGER NOT NULL
);

CREATE TABLE "Users" (
    id SERIAL,
    username CHARACTER VARYING(30) NOT NULL,
    password CHARACTER VARYING(200) NOT NULL,
    created_at DATE,
    updated_at DATE,
    active BOOLEAN DEFAULT TRUE NOT NULL,
	background_image TEXT DEFAULT NULL,
    avatar_image TEXT DEFAULT NULL,
	subtitle TEXT DEFAULT NULL,
    privilege INTEGER DEFAULT 1 NOT NULL
);
--
-- Constraints
--

ALTER TABLE ONLY "Articles"
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "ArticleVotes"
    ADD CONSTRAINT articlevotes_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "Collections"
    ADD CONSTRAINT collections_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "Comments"
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "CommentVotes"
    ADD CONSTRAINT commentvotes_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT unique_username UNIQUE (username);

ALTER TABLE ONLY "Users"
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "Articles"
    ADD CONSTRAINT articles_user_id_fkey FOREIGN KEY (user_id) REFERENCES "Users"(id);

ALTER TABLE ONLY "ArticleVotes"
    ADD CONSTRAINT articlevotes_article_id_fkey FOREIGN KEY (article_id) REFERENCES "Articles"(id);

ALTER TABLE ONLY "ArticleVotes"
    ADD CONSTRAINT articlevotes_user_id_fkey FOREIGN KEY (user_id) REFERENCES "Users"(id);

ALTER TABLE ONLY "CollectionArticle"
    ADD CONSTRAINT collection_article_article_id_fkey FOREIGN KEY (article_id) REFERENCES "Articles"(id);

ALTER TABLE ONLY "CollectionArticle"
    ADD CONSTRAINT collection_article_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES "Collections"(id);

ALTER TABLE ONLY "Collections"
    ADD CONSTRAINT collections_user_id_fkey FOREIGN KEY (user_id) REFERENCES "Users"(id);

ALTER TABLE ONLY "Comments"
    ADD CONSTRAINT comments_article_id_fkey FOREIGN KEY (article_id) REFERENCES "Articles"(id);

ALTER TABLE ONLY "Comments"
    ADD CONSTRAINT comments_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES "Comments"(id);

ALTER TABLE ONLY "Comments"
    ADD CONSTRAINT comments_user_id_fkey FOREIGN KEY (user_id) REFERENCES "Users"(id);

ALTER TABLE ONLY "CommentVotes"
    ADD CONSTRAINT commentvotes_comment_id_fkey FOREIGN KEY (comment_id) REFERENCES "Comments"(id);

ALTER TABLE ONLY "CommentVotes"
    ADD CONSTRAINT commentvotes_user_id_fkey FOREIGN KEY (user_id) REFERENCES "Users"(id);


ALTER TABLE ONLY "Followers"
    ADD CONSTRAINT followers_followee_id_fkey FOREIGN KEY (followee_id) REFERENCES "Users"(id);

ALTER TABLE ONLY "Followers"
    ADD CONSTRAINT followers_follower_id_fkey FOREIGN KEY (follower_id) REFERENCES "Users"(id);

ALTER TABLE ONLY "Subscriptions"
    ADD CONSTRAINT subscriptions_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES "Collections"(id);

ALTER TABLE ONLY "Subscriptions"
    ADD CONSTRAINT subscriptions_user_id_fkey FOREIGN KEY (user_id) REFERENCES "Users"(id);

--
-- Indexes
--

CREATE INDEX articles_user_id ON "Articles"(user_id);
CREATE INDEX collections_user_id ON "Collections"(user_id);
CREATE INDEX commentvotes_user_id ON "CommentVotes"(user_id);

CREATE TABLE "Articles_FTI"(
    titleString CHARACTER VARYING(70),
    contentString text,
    id oid
);

-- CREATE INDEX "Articles_FTI_titleString_idx" ON "Articles_FTI"(titleString);
-- CREATE INDEX "Articles_FTI_contentString_idx" ON "Articles_FTI"(contentString);
-- CREATE INDEX "Articles_FTI_id_idx" ON "Articles_FTI"(id);
-- CREATE INDEX "Articles_FTI_idx" ON "Articles_FTI"(id);

--  CREATE TRIGGER "Articles_FTI_trigger" AFTER UPDATE OR INSERT OR DELETE
--       ON "Articles" FOR EACH ROW
--       EXECUTE PROCEDURE fti("Articles_FTI", title, content);

--
-- Triggers
--

DROP TRIGGER IF EXISTS addCreatedAtComments ON "Comments";
DROP TRIGGER IF EXISTS addCreatedAtArticles ON "Articles";
DROP TRIGGER IF EXISTS addCreatedAtCollections ON "Collections";
DROP TRIGGER IF EXISTS addCreatedAtUsers ON "Users";
DROP TRIGGER IF EXISTS checkValueArticleVotes ON "ArticleVotes";
DROP TRIGGER IF EXISTS addCreatedAtCommentsVotes ON "CommentVotes";
DROP FUNCTION IF EXISTS checkValue();
DROP FUNCTION IF EXISTS addCreatedAt();

CREATE FUNCTION checkValue() RETURNS trigger AS $new$
    BEGIN
        IF NEW.value != 1 AND NEW.value != -1 THEN
            RAISE EXCEPTION 'value tem de ser +1 ou -1';
        END IF;
    END;
$new$ LANGUAGE plpgsql;

CREATE FUNCTION addCreatedAt() RETURNS trigger AS $new$
    BEGIN
       NEW.created_at = now();
       RETURN NEW;
    END;
$new$ LANGUAGE plpgsql;

CREATE TRIGGER addCreatedAtComments
    AFTER INSERT ON "Comments"
    FOR EACH ROW EXECUTE PROCEDURE addCreatedAt();

CREATE TRIGGER addCreatedAtArticles
    AFTER INSERT ON "Articles"
    FOR EACH ROW EXECUTE PROCEDURE addCreatedAt();

CREATE TRIGGER addCreatedAtCollections
    AFTER INSERT ON "Collections"
    FOR EACH ROW EXECUTE PROCEDURE addCreatedAt();

CREATE TRIGGER addCreatedAtUsers
    AFTER INSERT ON "Users"
    FOR EACH ROW EXECUTE PROCEDURE addCreatedAt();

CREATE TRIGGER checkValueArticleVotes
    AFTER INSERT ON "ArticleVotes"
    FOR EACH ROW EXECUTE PROCEDURE checkValue();

CREATE TRIGGER checkValueCommentVotes
    AFTER INSERT ON "CommentVotes"
    FOR EACH ROW EXECUTE PROCEDURE checkValue();


/*inserts*/

INSERT INTO "Users" ("id", "username", "password", "created_at", "updated_at", "active", "background_image", "avatar_image", "subtitle", "privilege") VALUES
(1, 'wabl', '698dc19d489c4e4db73e28a713eab07b', '2014-05-19',   NULL,   't',    'http://www.hdwallpapers.in/walls/abstract_color_background_picture_8016-wide.jpg', 'http://turizmkalbi.com/wp-content/uploads/upme/1399981295_658f2039885a85cc03cc31e20919bed6.png',   'Out of the shadows and into the light',    1);

INSERT INTO "Articles" ("id", "title", "content", "created_at", "updated_at", "user_id", "active", "imageurl") VALUES
(2, 'Toi rat chan', 'Duis purus turpis, faucibus quis ullamcorper quis, pretium et nulla. Donec consectetur eleifend mollis. Curabitur in auctor lorem. Sed leo nibh, gravida eu malesuada a, cursus sed nisl. Morbi suscipit vehicula aliquam. Mauris vitae facilisis ligula. Nullam fringilla massa et volutpat scelerisque. Vivamus non rhoncus metus.

Cras eget tempor risus, nec lacinia metus. Nulla fringilla velit id elit rhoncus, et consectetur nulla interdum. Vivamus eu tellus odio. Donec id lacus quis odio tristique suscipit aliquam at dui. Mauris ac ante in dolor gravida laoreet. Proin sodales consequat neque lacinia ullamcorper. Ut viverra volutpat eleifend.',    '2014-05-26',   NULL,   1,  't',    'http://www.neyralaw.com/wp-content/uploads/2012/07/tokyo-blue-background-4547.jpg'),
(1, 'Lorem Ipsum',  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam elit odio, tempor nec consequat eu, aliquet eget neque. Vestibulum sapien augue, tincidunt sit amet felis vel, auctor dignissim nulla. Sed sagittis blandit vestibulum. Ut at mauris eu erat ornare vestibulum. Sed mauris eros, laoreet porttitor est non, pharetra consequat turpis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Morbi consequat lacus mi, in vestibulum dolor dictum a. Maecenas quis risus quis tellus venenatis tristique. Morbi sed massa id mauris fringilla aliquet. Suspendisse id dolor adipiscing nisl tempus condimentum. Cras tincidunt placerat diam, in semper purus convallis et. Curabitur facilisis lectus nibh, at consequat leo gravida ultrices. Morbi a cursus mi. Pellentesque tristique ipsum non odio feugiat, nec dictum urna rhoncus. Vestibulum pharetra gravida dapibus. Aliquam malesuada vehicula elit, vitae sodales metus.

Donec massa orci, blandit quis cursus vitae, cursus ut quam. Nam sit amet lorem laoreet, aliquet sapien et, tincidunt mauris. Mauris tincidunt, sapien sit amet malesuada congue, odio neque ultricies nisl, in tempor metus turpis vel erat. Donec orci erat, mollis sit amet gravida tincidunt, mattis sed lacus. Nam sagittis tincidunt ligula, et pretium dolor molestie vel. In at adipiscing dolor. Phasellus adipiscing dignissim massa. Praesent ac commodo nulla. Maecenas rutrum magna et odio accumsan porttitor in eu sapien. Curabitur massa arcu, tristique in imperdiet ac, aliquam sit amet nibh. Donec faucibus rhoncus turpis, ac dignissim odio auctor vitae. Integer enim mauris, rutrum ac mi sed, tincidunt consectetur eros. In feugiat convallis quam sed luctus. Morbi sem est, feugiat nec felis eget, pellentesque porttitor libero.

Nunc vulputate, eros sed dictum molestie, turpis dolor pretium dui, id tincidunt tortor massa nec mi. Integer vulputate dolor sit amet tellus consectetur sodales. Praesent sapien lacus, vestibulum volutpat massa id, consequat commodo elit. Morbi tempor eu sem eget euismod. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel imperdiet lorem. Nunc tempor elit massa, quis dapibus urna sollicitudin sed. In hac habitasse platea dictumst.

Ut at diam sit amet neque congue facilisis. Curabitur a tempus quam, ac posuere libero. Vivamus nisi magna, condimentum at tincidunt et, dignissim eget metus. Phasellus vel tortor nec ipsum egestas lobortis sit amet et lorem. Donec non ligula sit amet lacus vulputate tempus. Ut malesuada malesuada magna vitae placerat. Integer fermentum placerat imperdiet. Mauris auctor at lacus at scelerisque. Duis malesuada nisi id felis hendrerit semper. Nunc ac lacus ut arcu vulputate pharetra tincidunt eu ipsum. In venenatis ante in lacus cursus, sed commodo quam porta. Sed orci arcu, egestas non dignissim sit amet, euismod facilisis nunc. Donec enim turpis, vulputate sit amet bibendum sit amet, mollis in leo. Sed sapien est, gravida eleifend odio eu, mattis scelerisque magna. Sed a quam non erat gravida pretium sed et libero. Nunc vitae ligula mattis, hendrerit elit vel, sollicitudin justo.',  '2014-05-19',   NULL,   1,  't',    'http://myclone.files.wordpress.com/2011/05/machinetop_background.jpg');

INSERT INTO "Collections" ("id", "name", "description", "user_id", "created_at", "updated_at", "imageurl") VALUES
(1, 'Primeira Coleccao',    'Vamos ver o que sai daqui',    1,  '2014-05-26',   NULL,   'http://vocedeolhoemtudo.com.br/wp-content/gallery/background/background-14.jpg');

INSERT INTO "CollectionArticle" ("collection_id", "article_id") VALUES
(1, 1),
(1, 2);